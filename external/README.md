This directory contains a series of external packages/libraries that
can be called from project library. Same packages/libraries are freely
available other not.

Each directory contains a series of "empty" subroutines written with
the only purpouse of making the library work in any case.

Those libraries that can be accessed via git should be included as
gitsubmodules (we are still thinking if this is a good way to work).