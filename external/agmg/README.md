In this directory we include the AGgregation Algebraic Multigrid library (AGMG)
available for free at this [address](http://www.agmg.eu/) in the linalg project.

Get the lastes release of the AGMG software (it is free under Academic licence)
and copy the AGMG-(version)/ directory here. (This procedure has been tested
with AGMG_3.3.5-aca on 21st March 2021)

If you can not/are not interested in using this algebraic multigrid
software just ignore everything. In the the dummy_src/directory we
wrote a series of subroutine that just have the same interface of the
main AGMG subroutines and return errors in case they are invoked.  We
adpot this procedure to make the other functionalities of the linalg
library available in any case.

