!>---------------------------------------------------------------------
!>
!> \author{Enrico Facca}
!>
!> DESCRIPTION: 
!> Compute matrix-vector multiplication or matrix transpose-vector multiplication
!> for timedata vector and fixed matrix
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM multiply_matrix_vector
  use Globals
  use TimeInputs
  use TimeOutputs
  use Matrix
  use SparseMatrix
  use DenseMatrix
    
  implicit none
  
  type(densemat),target :: matrix_a_full
  type(spmat),  target :: matrix_a_sparse
  class(abs_matrix), pointer :: matrix_a
  type(TimeData) :: pot
  type(TDOut)  :: grad

  logical :: rc
  logical :: in_cycle
  logical :: endfile,endfile2,endreading

  integer :: dim, ndata, npot, ngrad,nargs,narg,info,lun_err=0
  
  integer :: stderr,stdout,lun,res,res_read,lun_stat,i,j,itime
   
  type(file) :: file_matrix,  file_pot, file_grad


  character(len=256) :: arg
  character(len=256) :: out_format
  character(len=256) :: msg,str,fname,path_grad,path_matrix,path_pot
  character(len=1) :: direction

  real(kind=double) , allocatable :: vec_in(:),vec_out(:)

  real(kind=double) :: time


  stderr = 0

  nargs = command_argument_count() 
  write(*,*) nargs
  narg = 0
  if (nargs .lt. 3 ) call err_handle(stderr,narg)
    
  !
  ! matrix
  !
  narg=1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_matrix
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  !
  ! direction 
  !
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     str=to_lower(etb(arg))
     direction=str(1:1)
     write(*,*) etb(direction)
     if ( .not. ( (direction .eq. 'n') .or. (direction .eq. 't') ) )then
        rc = IOerr(stderr, err_inp, 'multiply_matrix_vector', &
             ' Errors read argument number =',narg)
        call err_handle(stderr,narg)
     end if
  end if


  !
  ! x 
  !
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) then
     call err_handle(stderr,narg)
  else
     read(arg,'(a256)',iostat=res_read) path_pot
     if (res_read .ne. 0) call err_handle(stderr,narg)
  end if

  
  !
  ! Ax
  !
  narg=narg+1
  call  get_command_argument(narg, arg,status=res)
  if (res .ne. 0) call err_handle(stderr,narg)
  read(arg,'(a256)',iostat=res_read) path_grad
  if (res_read .ne. 0) call err_handle(stderr,narg)
  

  !
  ! open files
  !
  call file_matrix%init(stderr,etb(path_matrix),10,'in')
  call file_pot%init(stderr,etb(path_pot),11,'in')
  call file_grad%init(stderr,etb(path_grad),12,'out')


  !
  ! try to interpret matrix type from head 
  ! TODO find something better than this approach
  ! 
  fname=file_matrix%fn
  lun=file_matrix%lun
  read(file_matrix%lun,*,iostat=res) i,j
  if (res.eq.0) then
     !
     ! its is full matrix
     !
     call file_matrix%kill(stderr)
     call file_matrix%init(stderr,fname, lun,'in')
     call matrix_a_full%read(stderr,lun)
     matrix_a => matrix_a_full 
  else
     !
     ! its is sparse
     !
     call file_matrix%kill(stderr)
     call file_matrix%init(stderr,fname, lun,'in')
     call matrix_a_sparse%read(stderr,file_matrix)
     matrix_a_sparse%storage_system='csr'
     matrix_a => matrix_a_sparse
  end if

  

  !
  ! get dimension data by the head
  !
  call pot%init(stderr,file_pot)
  npot=pot%ndata
  
  !
  ! check consistency
  !
  if ( (direction .eq. 'n') .and. (npot .ne. matrix_a%ncol) ) then
     rc = IOerr(stderr, err_inp , 'main', &
          ' dimensions mismacth ')
  else
     ngrad=matrix_a%nrow
  end if
  if ( (direction .eq. 't') .and. (npot .ne. matrix_a%nrow) ) then
     rc = IOerr(stderr, err_inp , 'main', &
          ' dimensions mismacth ')
  else
     ngrad=matrix_a%ncol
  end if
 
  allocate(&
       vec_in(npot),&
       vec_out(ngrad),&
       stat=res)
  if(res .ne. 0) rc = IOerr(stderr, err_alloc , 'main', &
         ' temp arrays vec_in vec_out',res)

  
  !
  ! init grad and associated file
  !
  dim=1
  if (direction .eq. 't') ndata = matrix_a%ncol
  if (direction .eq. 'n') ndata = matrix_a%nrow

  call grad%init(stderr,dim ,ndata)
  write(file_grad%lun,*) dim, ndata,' ! dim data' 

  !
  ! read data and write grad
  !
  endreading=.false.
  time  = pot%TDtime(1)
  itime = 0
  do while ( .not. endreading )
     !
     ! read data
     !
     call pot%set(stderr, file_pot, time, endfile2)     
     do i=1,npot
        vec_in(i)=pot%TDactual(1,i)
     end do

     !
     ! compute matrix vector operation
     !
     if (direction .eq. 't') call matrix_a%MTxv(vec_in,vec_out,info,lun_err)
     if (direction .eq. 'n') call matrix_a%Mxv(vec_in,vec_out,info,lun_err)
     grad%TDactual(1,:)=vec_out(:)
     grad%time=time
     grad%steadyTD=pot%steadyTD

     !
     ! write to file
     ! 
     call grad%write2dat(file_grad%lun)

     
     !
     ! exit if file ended, otherwise read next time
     !
     if (  endfile2 .or. pot%steadyTD) then
        endreading=.True.
        exit
     end if
     time = pot%TDtime(2)
  end do
  
  call file_pot%kill(stderr)
  call file_grad%kill(stderr) 

  deallocate(&
       vec_in,&
       vec_out,&
       stat=res)
  if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'main', &
         ' temp arrays vec_in vec_out',res)


end PROGRAM multiply_matrix_vector


subroutine err_handle(lun_err, narg)
  use Globals
  implicit none

  integer, intent(in) :: lun_err
  integer, intent(in) :: narg
  ! local
  logical :: rc
  write(lun_err,*) ' Error passing arguments, correct usage:'
  write(lun_err,*) ' ./mxv.out <matrix> <direction> <x> <y>'
  write(lun_err,*) ' where '
  write(lun_err,*) ' matrix    (in )      : triangulation in ascii'
  write(lun_err,*) ' direction (in )      : N or n -> y=Ax  '
  write(lun_err,*) '                      : T or t -> y=ATx  '
  write(lun_err,*) ' x         (in )      : timedata input'
  write(lun_err,*) ' y         (out)      : timedata output with y=Ax or y=ATx'
  rc = IOerr(lun_err, err_inp, 'main', &
       ' Errors read argument number =',narg)
end subroutine err_handle
