module ComposedMatrix
  use Globals
  use Matrix
  implicit none
  private
  !>----------------------------------------------------------------
  !> Structure variable to build implcit matrix given as composition
  !> of linear operator
  !>----------------------------------------------------------------
  type, extends(abs_matrix), public :: compose_mat
     !> Number of Matrices
     integer :: nmats     
     !> Pointer to matrices
     type(array_mat), allocatable :: mats(:)
     !> Flag for transposition
     character(len=1), allocatable :: direction(:)
     !> Maximum of matrices dimensions
     integer :: nscr
     !> Scratch array in matrix vector multiplications
     real(kind=double), allocatable :: scrin(:)
     !> Scratch array in matrix vector multiplications
     real(kind=double), allocatable :: scrout(:)

   contains
     !> static constructor
     !> (procedure public for type compose_mat)
     procedure, public, pass :: init => init_compose_mat
     !> static destructor
     !> (procedure public for type compose_mat)
     procedure, public, pass :: kill => kill_compose_mat
     !> Procedure to compute 
     !>         y = M * x 
     !> (public procedure for type blockmat)
     procedure, public, pass :: Mxv => Mxv_compose_mat
     !> Procedure to compute 
     !>         y = M^T * x 
     !> with M^T the transposed of a matrix M
     !> (public procedure for type blockmat)
     procedure, public, pass :: MTxv => MTxv_compose_mat
  end type compose_mat
contains
  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type compose_mat)
  !> Instantiate variable of type compose_mat
  !>
  !> usage:
  !>     call 'var'%init(lun_err, nrow, nnzblk )
  !>
  !> where:
  !> \param[in] lun_err               -> integer. Error logical unit
  !> \param[in] nmats                 -> integer. Number of rows 
  !> \param[in] nrow_block            -> integer. Number of columns
  !> \param[in] ncol_block            -> integer. Number of columns
  !> \param[in] nnzblk                -> integer. Number of non-zero term
  !>                                     stored in the matrix
  !> \param[in] block_structure       -> integer. dimension(4,nnzblock)
  !>                                      Table describing the block structure
  !> \param[in] is_symmetric          -> Logical. Flag for symmetric or not matrix
  !> \param[in] is_symmetric          -> integer. Dimension of the kernel
  !<-------------------------------------------------------------
  ! TODO automatic detection of symmetry or unsymmetric 
  subroutine init_compose_mat(this, lun_err, &
       nmats, mats,direction,&
       is_symmetric, triangular)
    use Globals
    implicit none
    !var
    class(compose_mat), intent(inout) :: this
    integer,            intent(in   ) :: lun_err
    integer,            intent(in   ) :: nmats
    type(array_mat),    intent(in   ) :: mats(nmats)
    character(len=1),   intent(in   ) :: direction(nmats)
    logical,            intent(in   ) :: is_symmetric
    character,          intent(in   ) :: triangular

    ! local vars
    logical :: rc
    integer :: res
    integer :: imat,nin,nout,nscr

    this%nmats        = nmats
    this%is_symmetric = is_symmetric
    this%triangular   = triangular

    write(*,*) this%nmats
    !
    ! check dimensions
    !
    do imat = nmats,2,-1
       select case (direction(imat))
       case ('N')
          nout = mats(imat)%mat%nrow 
       case ('T')
          nout  = mats(imat)%mat%ncol
       end select

       select case (direction(imat-1))
       case ('N')
          nin = mats(imat-1)%mat%ncol 
       case ('T')
          nin  = mats(imat-1)%mat%nrow
       end select
       call mats(imat)%mat%info(6)
       call mats(imat-1)%mat%info(6)
       if ( nout .ne. nin ) &
          rc = IOerr(lun_err, err_val, 'init_compose_mat', &
          ' Dimension mismatch in matrix =', imat)   
    end do
    
    
    allocate(&
         this%mats(nmats),&
         this%direction(nmats),&
         stat=res)
    if (res .ne. 0) &
         rc = IOerr(lun_err, err_alloc, 'init_compose_mat', &
         ' type compose_mat member mats')    
    this%mats      = mats
    this%direction = direction
    


    if ( this%direction(1) .eq. 'N') then
       this%nrow = mats(1)%mat%nrow
    else
       this%nrow = mats(1)%mat%ncol
    end if

    if ( this%direction(nmats) .eq. 'N') then
       this%ncol = mats(nmats)%mat%ncol
    else
       this%ncol = mats(nmats)%mat%nrow
    end if

    !
    ! allocate scratch arrays for intermediate 
    ! multiplication. We use the maximal dimensions
    ! of the matrices
    !
    nscr=0
    do imat=1,nmats
       nscr=max(nscr,mats(imat)%mat%nrow)
       nscr=max(nscr,mats(imat)%mat%ncol)
    end do
    this%nscr = nscr
    allocate(&
         this%scrin(nscr),&
         this%scrout(nscr),&
         stat=res)
    if ( res.ne. 0) &
         rc = IOerr(lun_err, err_alloc, 'init_compose_mat', &
         ' type compose_mat member scrin scrout')
    

  end subroutine init_compose_mat

  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type compose_mat)
  !> Free memory and diassociate pointers
  !>
  !> usage:
  !>     call 'var'%init(lun_err)
  !>
  !> where:
  !> \param[in] lun_err               -> integer. Error logical unit
  !<-------------------------------------------------------------
  subroutine kill_compose_mat(this, lun_err)
    use Globals
    implicit none
    !var
    class(compose_mat),   intent(inout) :: this
    integer,           intent(in   ) :: lun_err
    ! local vars
    logical :: rc
    integer :: res

    
    this%nrow  = 0
    this%ncol  = 0
    
    this%nmats = 0
    deallocate(&
         this%mats,&
         this%direction,&
         stat=res)
    if (res .ne. 0) &
         rc = IOerr(lun_err, err_dealloc, 'kill_compose_mat', &
         ' type compose_mat member mats direction')

    this%nscr  = 0
    deallocate(&
         this%scrin,&
         this%scrout,&
         stat=res)
    if (res .ne. 0) &
    rc = IOerr(lun_err, err_dealloc, 'kill_blocksmat', &
         ' type compose_mat member scrin, scrout')

  end subroutine kill_compose_mat

  !>-------------------------------------------------------------
  !> Abstract procedure defining the interface for a general
  !> matrix-vector multiplication
  !>         vec_out = (M) times (vec_in)
  !> (public procedure for class abs_matrix)
  !> 
  !> usage:
  !>     call 'var'%Mxv(vec_in,vec_out,[info])
  !>
  !> where 
  !> \param[in   ] vec_in          -> real, dimension('var'%ncol)
  !>                                   vector to be multiplied
  !> \param[inout] vec_out         -> real, dimension('var'%nrow)
  !>                                    vector (M) times (vec_in) 
  !> \param[in   ] (optional) info -> integer. Info number
  !>                                  in case of error   
  !<-------------------------------------------------------------
  recursive subroutine Mxv_compose_mat(this,vec_in,vec_out,info,lun_err)
    use Globals
    implicit none
    class(compose_mat), intent(inout) :: this
    real(kind=double),  intent(in   ) :: vec_in(this%ncol)
    real(kind=double),  intent(inout) :: vec_out(this%nrow)
    integer, optional,  intent(inout) :: info
    integer, optional,  intent(in   ) :: lun_err
           
    ! local
    integer imat,nin,nout


    this%scrin(1:this%ncol)  = vec_in(1:this%ncol)
    this%scrout(1:this%ncol) = zero
    do imat  = this%nmats,1,-1
       select case ( this%direction(imat) ) 
       case( 'N' )
          nin  = this%mats(imat)%mat%ncol
          nout = this%mats(imat)%mat%nrow
          call this%mats(imat)%mat%Mxv( &
               this%scrin(1:nin),&
               this%scrout(1:nout),&
               info)
          this%scrin(1:nout) = this%scrout(1:nout)
       case ('T') 
          nin  = this%mats(imat)%mat%nrow
          nout = this%mats(imat)%mat%ncol
          call this%mats(imat)%mat%MTxv(&
               this%scrin(1:nin),&
               this%scrout(1:nout),&
               info)
          this%scrin(1:nout) = this%scrout(1:nout)
       end select
    end do

    vec_out = this%scrout(1:this%nrow) 

  end subroutine Mxv_compose_mat
  !>-------------------------------------------------------------
  !> Abstract procedure defining the interface for a general
  !> matrix(transpose)-vector multiplication
  !>         vec_out = (M)^T times (vec_in)
  !> (public procedure for class abs_matrix)
  !> 
  !> usage:
  !>     call 'var'%MTxv(vec_in,vec_out,[info])
  !>
  !> where 
  !> \param[in   ] vec_in          -> real, dimension('var'%nrow)
  !>                                   vector to be multiplied
  !> \param[inout] vec_out         -> real, dimension('var'%ncol)
  !>                                    vector (M) times (vec_in) 
  !> \param[in   ] (optional) info -> integer. Info number
  !>                                  in case of error   
  !<-------------------------------------------------------------
  recursive subroutine MTxv_compose_mat(this,vec_in,vec_out,info,lun_err)
    use Globals
    implicit none
    class(compose_mat), intent(inout) :: this
    real(kind=double),  intent(in   ) :: vec_in(this%nrow)
    real(kind=double),  intent(inout) :: vec_out(this%ncol)
    integer, optional,  intent(inout) :: info
    integer, optional,  intent(in   ) :: lun_err

    !local
    integer :: imat,nin,nout


    this%scrin(1:this%nrow)  = vec_in(1:this%nrow)
    !this%scrout(1:this%ncol) = zero
    do imat  = 1, this%nmats
       select case ( this%direction(imat) ) 
       case( 'N' )
          nin  = this%mats(imat)%mat%nrow
          nout = this%mats(imat)%mat%ncol
          write (0,*) this%direction(imat) , nin,nout
          call this%mats(imat)%mat%MTxv(&
               this%scrin(1:nin),&
               this%scrout(1:nout),&
               info)
          this%scrin(1:nout) = this%scrout(1:nout)
       case ('T') 
          nin  = this%mats(imat)%mat%ncol
          nout = this%mats(imat)%mat%nrow
          write (0,*) this%direction(imat) , nin,nout

          call this%mats(imat)%mat%Mxv(&
               this%scrin(1:nin),&
               this%scrout(1:nout),&
               info)
          this%scrin(1:nout) = this%scrout(1:nout)
       end select
    end do

    vec_out = this%scrout(1:this%ncol) 


  end subroutine MTxv_compose_mat
end module ComposedMatrix

