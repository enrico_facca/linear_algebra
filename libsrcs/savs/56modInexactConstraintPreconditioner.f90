module InexactConstraintPreconditioner
  use Globals
  use Matrix
  use LinearOperator
  use FullPreconditioner
  use SimpleMatrix
  use SparseMatrix
  use StdSparsePrec
  use LinearSolver
  implicit none
  private
  !> Structure variable for the application of the 
  !> Inexact Constrained Preconditioner for Saddle point Matrix
  !> M= ( A B^T )
  !>    ( B C   )
  !> with A:= Real matrix of dimension na times na 
  !>      B:= Real matrix of dimension nb times na (constraint matrix) 
  !>      C:= Real matrix of dimension nb times nb
  !> in the form
  !> P ^-1 = ( I  -E1^-1 B^T ) ( E1^-1   0   ) ( I         0)
  !>         ( 0         I   ) ( 0      PS^-1) ( -E1^-1 B^T I)
  !> with
  !>      E1^-1 ~= A^{-1}
  !>      E2^-1 ~= A^{-1}
  !>      PS^-1 ~= S^{-1} 
  !> where S=B E2^{-1} B^T-C (the approximated Schur complement of A ) 
  !>------------------------------------------------------------------------
  type, extends(abs_linop), public :: constrained_prec
     !> Dimension of the 1-1 block of matrix M ( matrix A ) 
     integer :: na=0
     !> Dimension of the 2-2 block of matrix M (  matrix C)
     integer :: nb=0
     !> Matrix B 
     class(abs_matrix), pointer :: matrix_B => null()
     !> Preconditioner E
     class(abs_linop), pointer :: prec_E1 => null()
     !> prec_shur 
     class(abs_linop), pointer :: prec_Schur => null()
     !> Real aux variables
     real(kind=double) , allocatable :: prec_E1_a(:)
     
     real(kind=double) , allocatable :: vec_d(:)
     
     real(kind=double) , allocatable :: vec_temp(:)
   contains
     !> static constructor
     !> (procedure public for type constrained_prec)
     procedure, public, pass :: init => init_constrained_prec
     !> static constructor
     !> (procedure public for type constrained_prec)
     procedure, public, pass :: set => set_constrained_prec
     !> static destructor
     !> (procedure public for type constrained_prec)
     procedure, public, pass :: kill => kill_constrained_prec
     !> static destructor
     !> (procedure public for type constrained_prec)
     procedure, public, pass :: info => info_constrained_prec
     !> Procedure to compute 
     !>         y = P^{-1} * x 
     !> (public procedure for type diag_prec)
     procedure, public,  pass :: Mxv => apply_constrained_prec
  end type constrained_prec


  type, extends(abs_linop), public :: mixed_constrained_prec
     !> Dimension of the 1-1 block of matrix M ( matrix A ) 
     integer :: na=0
     !> Dimension of the 2-2 block of matrix M (  matrix C)
     integer :: nb=0
     !> Matrix B 
     class(abs_matrix), pointer :: matrix_B => null()
     !> Preconditioner E
     class(abs_linop), pointer :: prec_E1 => null()
     !> prec_shur 
     class(abs_linop), pointer :: prec_Schur => null()
     !> Real aux variables
     real(kind=double) , allocatable :: prec_E1_a(:)
     
     real(kind=double) , allocatable :: vec_d(:)
     
     real(kind=double) , allocatable :: vec_temp(:)
     
     real(kind=double) , allocatable :: deltatD1(:)
   contains
     !> static constructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: init => init_mixed_constrained_prec
     !> static constructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: set => set_mixed_constrained_prec
     !> static destructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: kill => kill_mixed_constrained_prec
     !> Procedure to compute 
     !>         y = P^{-1} * x 
     !> (public procedure for type diag_prec)
     procedure, public,  pass :: Mxv => apply_mixed_constrained_prec
  end type mixed_constrained_prec

  type, extends(abs_linop), public :: triangular_constrained_prec
     !> Dimension of the 1-1 block of matrix M ( matrix A ) 
     integer :: na=0
     !> Dimension of the 2-2 block of matrix M (  matrix C)
     integer :: nb=0
     !> Matrix B 
     class(abs_matrix), pointer :: matrix_B => null()
     !> Preconditioner E
     class(abs_linop), pointer :: prec_E1 => null()
     !> prec_shur 
     class(abs_linop), pointer :: prec_Schur => null()
     !> Real aux variables     
     real(kind=double) , allocatable :: vec_d(:)
     !> Real aux variables     
     real(kind=double) , allocatable :: deltatD1(:)
   contains
     !> static constructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: init => init_triangular_constrained_prec
     !> static constructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: set => set_triangular_constrained_prec
     !> static destructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: kill => kill_triangular_constrained_prec
     !> Procedure to compute 
     !>         y = P^{-1} * x 
     !> (public procedure for type diag_prec)
     procedure, public,  pass :: Mxv=> apply_triangular_constrained_prec
  end type triangular_constrained_prec

  !> type to define inverse of
  !> A B 
  !> 0 D
  type, extends(abs_linop), public :: augmented_prec
     !> Dimension of the 1-1 block of matrix M ( matrix A ) 
     integer :: na=0
     !> Dimension of the 2-2 block of matrix M (  matrix D)
     integer :: nb=0
     !> Inverser or approximate inverse of A_gamma matrix
     class(abs_linop), pointer :: inverse_A_gamma => null()
     !> Matrix B 
     class(abs_linop), pointer :: matrix_B => null()
     !> Inverser or approximate inverse of D matrix
     class(abs_linop), pointer :: inverse_D => null()
     !> Scr 
     real(kind=double) , allocatable :: vec_d(:)
   contains
     !> static constructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: init => init_augmented_prec
     !> static constructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: set => set_augmented_prec
     !> static destructor
     !> (procedure public for type mixed_constrained_prec)
     procedure, public, pass :: kill => kill_augmented_prec
     !> Procedure to compute 
     !>         y = P^{-1} * x 
     !> (public procedure for type diag_prec)
     procedure, public,  pass :: Mxv=> apply_augmented_prec
  end type augmented_prec


  contains
    !>-------------------------------------------------------------
    !> Static constructor.
    !> (procedure public for type stiffprec)
    !> Instantiate and initiliaze variable of type constrained_prec
    !>
    !> usage:
    !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
    !> where:
    !> \param[in] lun_err  -> integer. Error logical unit
    !> \param[in] matrix_B -> class(abs_matrix) Constraint matrix
    !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
    
    !<-------------------------------------------------------------
    subroutine init_constrained_prec(this,&
         lun_err,&
         matrix_B,&
         prec_E1_nequ, prec_Schur_nequ)
     use Globals
     implicit none
     class(constrained_prec),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     class(abs_matrix),  intent(in   ) :: matrix_B
     integer,            intent(in   ) :: prec_E1_nequ
     integer,            intent(in   ) :: prec_Schur_nequ

     
     !local
     logical :: rc
     integer :: res,i

     ! set properties
     this%na           = prec_E1_nequ
     this%nb           = prec_Schur_nequ
     this%nrow         = prec_E1_nequ + prec_Schur_nequ
     this%nrow         = this%ncol
     !
     allocate(&
          this%prec_E1_a(this%na),&
          this%vec_d(this%nb),&
          this%vec_temp(this%na),&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_alloc,&
          ' init_constrained_prec', &
          ' type constrained_prec memberprec_E1_a , vec_d, vec_temp ',res)
     
   end subroutine init_constrained_prec

   !>-------------------------------------------------------------
   !> Static constructor.
   !> (procedure public for type stiffprec)
   !> Instantiate and initiliaze variable of type constrained_prec
   !>
   !> usage:
   !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
   !> where:
   !> \param[in] lun_err  -> integer. Error logical unit
   !> \param[in] matrix_B -> class(abs_mat) Constraint matrix
   !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
   
   !<-------------------------------------------------------------
   subroutine set_constrained_prec(this,lun_err,matrix_B, prec_E1, prec_Schur)
     use Globals
     implicit none
     class(constrained_prec),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     class(abs_matrix) , target, intent(in) :: matrix_B
     class(abs_linop), target, intent(in) :: prec_E1
     class(abs_linop), target, intent(in) :: prec_Schur
     !local
     logical :: rc
     integer :: res

     this%is_symmetric = prec_E1%is_symmetric .and. prec_Schur%is_symmetric

     this%matrix_B => matrix_B
     this%prec_E1 => prec_E1
     this%prec_Schur => prec_Schur
     
   end subroutine set_constrained_prec


   subroutine kill_constrained_prec(this,lun_err)
     use Globals
     implicit none
     class(constrained_prec),  intent(inout) :: this
     integer,         intent(in   ) :: lun_err
     !local
     logical :: rc
     integer :: res

     this%is_symmetric   = .false.     

     ! set properties
     this%na           = 0
     this%nb           = 0

     !
     deallocate(&
          this%prec_E1_a,&
          this%vec_d,&
          this%vec_temp,&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_dealloc,&
          ' init_constrained_prec', &
          ' type constrained_prec memberprec_E1_a , vec_d, vec_temp ',res)
     
     
     this%matrix_B => null()
     this%prec_E1 => null()
     this%prec_Schur => null()

   end subroutine kill_constrained_prec

   subroutine info_constrained_prec(this,lun)
     use Globals
     use StdSparsePrec
     implicit none
     class(constrained_prec),  intent(in) :: this
     integer,         intent(in   ) :: lun

     write(lun,'(a,I8,I8)') 'Dim prec | dimension =', &
          this%nrow
     
     select type (var=>this%prec_Schur)
     type is ( stdprec ) 
        call var%info(lun)
     end select
        

   end subroutine info_constrained_prec



   subroutine apply_constrained_prec(this,vec_in,vec_out,info,lun_err)
     use Globals
     implicit none
     class(constrained_prec),   intent(inout) :: this
     real(kind=double), intent(in   ) :: vec_in(this%ncol)
     real(kind=double), intent(inout) :: vec_out(this%nrow)
     integer, optional, intent(inout) :: info
     integer, optional, intent(in   ) :: lun_err
     !local 
     integer :: info_loc

     ! compute vector $c=E^-1 a$
     call this%prec_E1%Mxv(vec_in(1:this%na),this%prec_E1_a,info_loc,lun_err)
     if ( info_loc .ne. 0) then
        if( present(info) ) info=info_loc
        return
     end if
     ! compute vector $ d=Bc$
     call this%matrix_B%Mxv(this%prec_E1_a, this%vec_d)

     ! B c - b in vec_d
     this%vec_d = this%vec_d - vec_in(1+this%na:this%nrow)


     ! aply Schur prec
     call this%prec_Schur%Mxv(this%vec_d, &
          vec_out(this%na+1:this%nrow),info_loc,lun_err)
     if ( info_loc .ne. 0) then
        if( present(info) ) info=info_loc
        return
     end if
     

     call this%matrix_B%MTxv(&
          vec_out(this%prec_E1%nrow+1:this%nrow), &
          this%vec_temp,&
          info)

     call this%prec_E1%Mxv(this%vec_temp,vec_out(1:this%na),info_loc,lun_err)
     if ( info_loc .ne. 0) then
        if( present(info) ) info=info_loc
        return
     end if

     vec_out(1:this%prec_E1%nrow) =&
          -vec_out(1:this%na) + this%prec_E1_a

     
     

     
   end subroutine apply_constrained_prec


   !>-------------------------------------------------------------
    !> Static constructor.
    !> (procedure public for type stiffprec)
    !> Instantiate and initiliaze variable of type mixed_constrained_prec
    !>
    !> usage:
    !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
    !> where:
    !> \param[in] lun_err  -> integer. Error logical unit
    !> \param[in] matrix_B -> class(abs_matrix) Constraint matrix
    !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
    
    !<-------------------------------------------------------------
    subroutine init_mixed_constrained_prec(this,&
         lun_err,&
         matrix_B,&
         prec_E1_nequ, prec_Schur_nequ)
     use Globals
     implicit none
     class(mixed_constrained_prec),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     class(abs_matrix),  intent(in   ) :: matrix_B
     integer,            intent(in   ) :: prec_E1_nequ
     integer,            intent(in   ) :: prec_Schur_nequ

     
     !local
     logical :: rc
     integer :: res,i

     ! set properties
     this%na           = prec_E1_nequ
     this%nb           = prec_Schur_nequ
     this%nrow         = prec_E1_nequ + prec_Schur_nequ
     this%ncol = this%nrow 
     !
     allocate(&
          this%prec_E1_a(this%na),&
          this%vec_d(this%nb),&
          this%vec_temp(this%na),&
          this%deltatD1(this%nb),&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_alloc,&
          ' init_mixed_constrained_prec', &
          ' type mixed_constrained_prec memberprec_E1_a , vec_d, vec_temp ',res)
     
   end subroutine init_mixed_constrained_prec

   !>-------------------------------------------------------------
   !> Static constructor.
   !> (procedure public for type stiffprec)
   !> Instantiate and initiliaze variable of type mixed_constrained_prec
   !>
   !> usage:
   !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
   !> where:
   !> \param[in] lun_err  -> integer. Error logical unit
   !> \param[in] matrix_B -> class(abs_mat) Constraint matrix
   !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
   
   !<-------------------------------------------------------------
   subroutine set_mixed_constrained_prec(this,lun_err,matrix_B, prec_E1, prec_Schur,deltatD1)
     use Globals
     implicit none
     class(mixed_constrained_prec),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     class(abs_matrix) , target, intent(in) :: matrix_B
     class(abs_linop), target, intent(in) :: prec_E1
     class(abs_linop), target, intent(in) :: prec_Schur
     real(kind=double), intent(in) :: deltatD1(matrix_B%nrow)
     !local
     logical :: rc
     integer :: res

     this%is_symmetric = prec_E1%is_symmetric .and. prec_Schur%is_symmetric

     this%matrix_B => matrix_B
     this%prec_E1 => prec_E1
     this%prec_Schur => prec_Schur
     this%deltatD1 = deltatD1
     
   end subroutine set_mixed_constrained_prec


   subroutine kill_mixed_constrained_prec(this,lun_err)
     use Globals
     implicit none
     class(mixed_constrained_prec),  intent(inout) :: this
     integer,         intent(in   ) :: lun_err
     !local
     logical :: rc
     integer :: res

     this%is_symmetric   = .false.     

     ! set properties
     this%na           = 0
     this%nb           = 0

     !
     deallocate(&
          this%prec_E1_a,&
          this%vec_d,&
          this%vec_temp,&
          this%deltatD1,&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_dealloc,&
          ' init_mixed_constrained_prec', &
          ' type mixed_constrained_prec memberprec_E1_a , vec_d, vec_temp ',res)
     
     
     this%matrix_B => null()
     this%prec_E1 => null()
     this%prec_Schur => null()

   end subroutine kill_mixed_constrained_prec

   subroutine info_mixed_constrained_prec(this,lun)
     use Globals
     use StdSparsePrec
     implicit none
     class(mixed_constrained_prec),  intent(in) :: this
     integer,         intent(in   ) :: lun

     write(lun,'(a,I8,I8)') 'Dim prec | dimension =', &
          this%nrow
     
     select type (var=>this%prec_Schur)
     type is ( stdprec ) 
        call var%info(lun)
     end select
        

   end subroutine info_mixed_constrained_prec



   subroutine apply_mixed_constrained_prec(this,vec_in,vec_out,info,lun_err)
     use Globals
     implicit none
     class(mixed_constrained_prec),   intent(inout) :: this
     real(kind=double), intent(in   ) :: vec_in(this%ncol)
     real(kind=double), intent(inout) :: vec_out(this%nrow)
     integer, optional, intent(inout) :: info
     integer, optional, intent(in   ) :: lun_err
     !
     integer :: info_loc,i
     real(kind=double) :: s
     info_loc = 0

     !     w = A^{-1} x
     !do i=1,this%na
     !   s=s+vec_in(i)
     !end do
     !write(*,*) 'sum', s

     ! this%prec_E1_a = A^{-1} vec_in(1:na)
     call this%prec_E1%Mxv(vec_in(1:this%na),this%prec_E1_a,info_loc,lun_err)
     if ( present(info) .and. ( info_loc .ne. 0 ) ) then 
        info=info_loc 
        return
     end if


     ! z = y+deltat*(D1*(B*w)) = (-C* A^{-1}x + I*y)
     call this%matrix_B%Mxv(this%prec_E1_a, this%vec_d, info_loc)
     this%vec_d(1:this%nb) = vec_in(1+this%na:this%ncol) + this%deltatD1 * this%vec_d
     if ( present(info) .and. ( info_loc .ne. 0 ) ) then 
        info=info_loc 
        return
     end if

     ! t = (S^-1) z ;
     ! pr(n+1:n+m) = t;
     call this%prec_Schur%Mxv(this%vec_d, &
          vec_out(this%na+1:this%ncol),info,lun_err)
     !vec_out(this%na+1:this%nrow) = - vec_out(this%na+1:this%nrow) 
     if ( present(info) .and. ( info_loc .ne. 0 ) ) then 
        info=info_loc 
        return
     end if


     !pr(1:n) = w - A^{-1} (B'*t);
     call this%matrix_B%MTxv(&
          vec_out(this%na+1:this%ncol), &
          this%vec_temp(1:this%na),&
          info_loc)
     if ( present(info) .and. ( info_loc .ne. 0 ) ) then 
        info=info_loc 
        return
     end if
     call this%prec_E1%Mxv(this%vec_temp,vec_out(1:this%na),info_loc,lun_err)
     if ( present(info) .and. ( info_loc .ne. 0 ) ) then 
        info=info_loc 
        return
     end if
     vec_out(1:this%na) = this%prec_E1_a(1:this%na) - vec_out(1:this%na)
    
   end subroutine apply_mixed_constrained_prec


   
   !>-------------------------------------------------------------
    !> Static constructor.
    !> (procedure public for type stiffprec)
    !> Instantiate and initiliaze variable of type triangular_constrained_prec
    !>
    !> usage:
    !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
    !> where:
    !> \param[in] lun_err  -> integer. Error logical unit
    !> \param[in] matrix_B -> class(abs_matrix) Constraint matrix
    !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
    
    !<-------------------------------------------------------------
    subroutine init_triangular_constrained_prec(this,&
         lun_err,&
         matrix_B,&
         prec_E1_nequ, prec_Schur_nequ)
     use Globals
     implicit none
     class(triangular_constrained_prec),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     class(abs_linop),  intent(in   ) :: matrix_B
     integer,            intent(in   ) :: prec_E1_nequ
     integer,            intent(in   ) :: prec_Schur_nequ

     
     !local
     logical :: rc
     integer :: res,i

     ! set properties
     this%na           = prec_E1_nequ
     this%nb           = prec_Schur_nequ
     this%nrow         = prec_E1_nequ + prec_Schur_nequ
     this%ncol = this%nrow
     !
     allocate(&
          this%vec_d(this%nb),&
          this%deltatD1(this%nb),&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_alloc,&
          ' init_triangular_constrained_prec', &
          ' type triangular_constrained_prec memberprec_E1_a , vec_d, vec_temp ',res)
     
   end subroutine init_triangular_constrained_prec

   !>-------------------------------------------------------------
   !> Static constructor.
   !> (procedure public for type stiffprec)
   !> Instantiate and initiliaze variable of type triangular_constrained_prec
   !>
   !> usage:
   !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
   !> where:
   !> \param[in] lun_err  -> integer. Error logical unit
   !> \param[in] matrix_B -> class(abs_mat) Constraint matrix
   !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
   
   !<-------------------------------------------------------------
   subroutine set_triangular_constrained_prec(this,lun_err,&
        matrix_B, prec_E1, prec_Schur,deltatD1)
     use Globals
     implicit none
     class(triangular_constrained_prec),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     class(abs_matrix) , target, intent(in) :: matrix_B
     class(abs_linop), target, intent(in) :: prec_E1
     class(abs_linop), target, intent(in) :: prec_Schur
     real(kind=double), intent(in) :: deltatD1(matrix_B%nrow)
     !local
     logical :: rc
     integer :: res

     this%is_symmetric = prec_E1%is_symmetric .and. prec_Schur%is_symmetric

     this%matrix_B => matrix_B
     this%prec_E1 => prec_E1
     this%prec_Schur => prec_Schur
     this%deltatD1 = deltatD1
     
   end subroutine set_triangular_constrained_prec


   subroutine kill_triangular_constrained_prec(this,lun_err)
     use Globals
     implicit none
     class(triangular_constrained_prec),  intent(inout) :: this
     integer,         intent(in   ) :: lun_err
     !local
     logical :: rc
     integer :: res

     this%is_symmetric   = .false.     

     ! set properties
     this%na           = 0
     this%nb           = 0

     !
     deallocate(&
          this%vec_d,&
          this%deltatD1,&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_dealloc,&
          ' init_triangular_constrained_prec', &
          ' type triangular_constrained_prec memberprec_E1_a , vec_d, vec_temp ',res)
     
     
     this%matrix_B => null()
     this%prec_E1 => null()
     this%prec_Schur => null()

   end subroutine kill_triangular_constrained_prec

   subroutine info_triangular_constrained_prec(this,lun)
     use Globals
     use StdSparsePrec
     implicit none
     class(triangular_constrained_prec),  intent(in) :: this
     integer,         intent(in   ) :: lun

     write(lun,'(a,I8,I8)') 'Dim prec | dimension =', &
          this%nrow
     
     select type (var=>this%prec_Schur)
     type is ( stdprec ) 
        call var%info(lun)
     end select
        

   end subroutine info_triangular_constrained_prec

   subroutine apply_triangular_constrained_prec(this,vec_in,vec_out,info,lun_err)
     use Globals
     implicit none
     class(triangular_constrained_prec),   intent(inout) :: this
     real(kind=double), intent(in   ) :: vec_in(this%ncol)
     real(kind=double), intent(inout) :: vec_out(this%nrow)
     integer, optional, intent(inout) :: info
     integer, optional, intent(in   ) :: lun_err

     !     w = LA'\(LA\r(1:n))
     call this%prec_E1%Mxv(vec_in(1:this%na),vec_out(1:this%na),info,lun_err)
     
     ! z = r(n+1:n+m)+deltat*(D1*(B*w));
     call this%matrix_B%Mxv(vec_out(1:this%na), this%vec_d, info)
     this%vec_d = ( vec_in(this%na+1:this%nrow) + this%deltatD1 * this%vec_d ) 

     ! t = U\(L\z);
     ! pr(n+1:n+m) = t;
     call this%prec_Schur%Mxv(this%vec_d, &
          vec_out(this%na+1:this%nrow),info,lun_err)
     
     
   end subroutine apply_triangular_constrained_prec


   !<-------------------------------------------------------------
    subroutine init_augmented_prec(this,&
         lun_err,&
         matrix_A_gamma,&
         matrix_B)
     use Globals
     implicit none
     class(augmented_prec), intent(inout) :: this
     integer,               intent(in   ) :: lun_err
     type(spmat),           intent(in   ) :: matrix_A_gamma
     class(abs_linop),      intent(in   ) :: matrix_B

     
     !local
     logical :: rc
     integer :: res,i

     this%na           = matrix_A_gamma%ncol
     this%nb           = matrix_B%nrow
     this%nrow         = matrix_B%ncol + matrix_B%nrow
     this%ncol         = matrix_B%ncol + matrix_B%nrow
     this%is_symmetric = .False.
     !
     allocate(&
          this%vec_d(this%na),&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_alloc,&
          ' init_augmented_prec', &
          ' type augmented_prec memberprec_E1_a , vec_d, vec_temp ',res)

   end subroutine init_augmented_prec

   !>-------------------------------------------------------------
   !> Static constructor.
   !> (procedure public for type stiffprec)
   !> Instantiate and initiliaze variable of type augmented_prec
   !>
   !> usage:
   !>     call 'var'%init(lun_err,matrix_B, prec_E1, prec_shur)
   !> where:
   !> \param[in] lun_err  -> integer. Error logical unit
   !> \param[in] matrix_B -> class(abs_mat) Constraint matrix
   !> \param[in] prec_E1   -> class(prec_schur) Constraint matrix  
   
   !<-------------------------------------------------------------
   subroutine set_augmented_prec(this,lun_err,&
        inverse_A_gamma, matrix_B, inverse_D_matrix)
     use Globals
     implicit none
     class(augmented_prec),      intent(inout) :: this
     integer,                    intent(in   ) :: lun_err
     class(abs_linop),      target, intent(in   ) :: inverse_A_gamma
     class(abs_linop),      target, intent(in   ) :: matrix_B
     class(abs_linop),      target, intent(in   ) :: inverse_D_matrix
     

     !local
     logical :: rc
     integer :: res
     type(input_prec) :: ctrl_prec
     type(input_solver) :: ctrl_solver
     

     this%inverse_A_gamma =>inverse_A_gamma
     this%matrix_B        => matrix_B
     this%inverse_D =>inverse_D_matrix

     
   end subroutine set_augmented_prec


   subroutine kill_augmented_prec(this,lun_err)
     use Globals
     implicit none
     class(augmented_prec),  intent(inout) :: this
     integer,         intent(in   ) :: lun_err
     !local
     logical :: rc
     integer :: res

     this%is_symmetric   = .false.     

     ! set properties
     this%na           = 0
     this%nb           = 0

     !
     deallocate(&
          this%vec_d,&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_dealloc,&
          ' init_augmented_prec', &
          ' type augmented_prec memberprec_E1_a , vec_d, vec_temp ',res)

     this%inverse_A_gamma => null()
     this%matrix_B        => null()
     this%inverse_D => null()
   end subroutine kill_augmented_prec

   !
   ! solve (A 0  ) (x) = (f)
   !       (B D  ) (y)   (g)

   subroutine apply_augmented_prec(this,vec_in,vec_out,info,lun_err)
     use Globals
     implicit none
     class(augmented_prec),   intent(inout) :: this
     real(kind=double), intent(in   ) :: vec_in(this%ncol)
     real(kind=double), intent(inout) :: vec_out(this%nrow)
     integer, optional, intent(inout) :: info
     integer, optional, intent(in   ) :: lun_err
     ! local
     logical :: rc
     real(kind=double) :: dnrm2
     integer :: i,info_inverse,res
     
     if( present(info) ) info=0

     ! init vec_out
     vec_out=zero

     ! x = A^{-1} f
     this%vec_d = vec_in
     call this%inverse_A_gamma%Mxv(this%vec_d(1:this%na), &
          vec_out(1:this%na),info_inverse,lun_err)
     if ( info_inverse .ne. 0) then
        if( present(info) ) info=info_inverse
        rc = IOerr(lun_err, wrn_val,&
             ' apply_augmented_prec', &
             ' application of inverse of A_gamma failed ',res)
        return
     end if
     

     
     ! z = g - B x = g - B A^{-1} f
     call this%matrix_B%Mxv(vec_out(1:this%na), this%vec_d(1:this%nb))
     do i=1,this%nb
        this%vec_d(i) = vec_in(this%na+i) - this%vec_d(i) 
     end do
     
     ! y = D^{-1} z
     call this%inverse_D%Mxv(&
          this%vec_d(1:this%nb),vec_out(this%na+1:this%nrow),info_inverse,lun_err)
     if ( info_inverse .ne. 0 ) then
        if( present(info) ) info=info_inverse
        rc = IOerr(lun_err, wrn_val,&
             ' apply_augmented_prec', &
             ' application of inverse of A_gamma failed ',res)
        return
     end if
     
     
   end subroutine apply_augmented_prec



 end module InexactConstraintPreconditioner
