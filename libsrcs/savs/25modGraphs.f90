module Graphs
  use Globals
  use Matrix
  use SparseMatrix
  implicit none
  private
  !> Derived structure for the description of topological
  !> graph, which is the list of nedge connections
  !> among nnode items( usually point in \REAL^3)
  type, public:: net
     !> Number of vertices
     integer :: nnode
     !> Number of ncells
     integer :: ncell
     !> Dimension(2,nedge)
     !> Connectivity of the edges
     integer, allocatable :: topol(:,:)
     !> Real Weight associated to edge
     !> Dimension(ncell)
     real(kind=double), allocatable :: weight(:)
     !> Real coordinate
     !> Dimension(3,nnode)
     real(kind=double), allocatable :: coord(:,:)
   contains
     !> static constructor
     !> (procedure public for type star)
     procedure, public, pass :: init => init_net
     !> static destructor
     !> (procedure public for type net)
     procedure, public, pass :: kill => kill_net
     !> Info procedure.
     !> (public procedure for type net)
     procedure, public, pass :: info => info_net
     !> Write procedure into data file
     !> (public procedure for type net)
     procedure, public, pass :: read => read_net 
     !> Write procedure into data file
     !> (public procedure for type net)
     procedure, public, pass :: write2dat => write2dat_net
     !> Procedure build a subgraoh
     !> selecting some nodes
     !> (public procedure for type net)
     procedure, public, pass :: select
  end type net
  !> Derived structure for the implicit definition
  !> of the signed adjency matrix A of a graph
  !> 
  !>  A_{iedde,:}= (0..1........0.......-1..0)
  !>                   |                 |
  !>                   n1(iedge)         n2(idege)
  type, extends(abs_matrix), public:: adjmat
     !Pointer to the graph
     type(net), pointer :: graph
   contains
     !> static constructor
     !> (procedure public for type adjmat)
     procedure, public, pass :: init => init_adjmat
     !> static destructor
     !> (procedure public for type adjmat)
     procedure, public, pass :: kill => kill_adjmat
     !> Info procedure.
     !> (public procedure for type adjmat)
     procedure, public, pass :: info => info_adjmat
     !> Matrix Vector Procedure
     !> (public procedure for type adjmat)
     procedure, public, pass :: Mxv
     !> Matrix Vector Procedure
     !> (public procedure for type adjmat)
     procedure, public, pass :: MTxv
     !> Prepare stiff matrix and assembler to build
     !> A^T D A
     !> (public procedure for type adjmat)
     procedure, public, pass :: preassembly_stiff
     !> Assembly sparse matrix
     !> A^T D A
     !> (public procedure for type adjmat)
     procedure, public, pass :: assembly_stiff
  end type adjmat
contains
  subroutine init_net(this,&
       stderr,nnode,ncell,&
       topol)
    use Globals
    class(net), intent(inout) :: this
    integer,    intent(in   ) :: stderr
    integer,    intent(in   ) :: nnode
    integer,    intent(in   ) :: ncell
    integer,    intent(in   ) :: topol(2,ncell)
    !local
    logical :: rc
    integer :: res,j,iedge,itria1,itria2

    this%nnode = nnode
    this%ncell = ncell

    allocate (&
         this%topol(2,ncell),&
         stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_net', &
         ' type net meber topol, coord',res)

    this%topol = topol 
    
  end subroutine init_net

  

  subroutine read_net(this, lun_err, file2read)
    use Globals
    implicit none
    !vars
    class(net),    intent(out) :: this
    integer,        intent(in ) :: lun_err
    type(file),     intent(in ) :: file2read
    ! local vars
    integer :: u_number
    integer :: j, k, res, ncoord,nnodeincell
    logical :: rc
    character(len=256) :: rdwr,str,fname,first_line

    u_number    = file2read%lun
    fname       = file2read%fn
    nnodeincell = 2

    read(u_number,*,iostat=res) this%nnode
    if(res .ne. 0) rc = IOerr(lun_err, err_inp , 'read_net', &
         etb(fname) // ' vars nnode',res)

    read(u_number,*,iostat=res) this%ncell
    if(res .ne. 0) rc = IOerr(lun_err, err_inp , 'read_net', &
         etb(fname) // ' vars ncell',res)

    allocate(this%coord(3,this%nnode),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'read_net', &
         '  type net member coord (array)',res)
    allocate(this%topol(nnodeincell+1,this%ncell),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'read_net', &
         '  type net member triangles (array)',res)

    ! read first line to check if the the third coordinate is present 
    read(u_number,'(a)',iostat=res) first_line
    ncoord=3
    read(first_line,*,iostat=res) (this%coord(k,1),k=1,ncoord)
    if (res .ne. 0) THEN
       this%coord(3,:) = zero
       ! read only first two column, the first is initialized to zero
       ncoord = 2
       read(first_line,*,iostat=res) (this%coord(k,1),k=1,ncoord)
       if(res .ne. 0) THEN
          write(rdwr,'(i5)') 1
          str=etb(rdwr)//'/'
          rc = IOerr(lun_err, err_inp , 'read_net', &
               trim(etb(fname)) //&
               ' type net member array coord at line '//&
               trim(str),res)
       end if
    end if
    
    do j=2,this%nnode
       read(u_number,*,iostat=res) (this%coord(k,j),k=1,ncoord)
       if(res .ne. 0) THEN
          write(rdwr,'(i5)') j
          str=etb(rdwr)//'/'
          rc = IOerr(lun_err, err_inp , 'read_net', &
               trim(etb(fname)) //&
               ' type net member array coord at line '//&
               trim(str),res)
       end if
    end do

    
    do j=1,this%ncell
       read(u_number,*,iostat=res) (this%topol(k,j), k=1,nnodeincell)
       if(res .ne. 0) then
          write(rdwr,'(i5)') j
          str=etb(rdwr)//'/'
          write(rdwr,'(i5)') k
          str=trim(str)//etb(rdwr)
          rc = IOerr(lun_err, err_inp , 'read_net', &
               trim(etb(fname)) // &
               ' type net member array triang at line/col '//trim(str),res)
       end if
    end do
    
  end subroutine read_net

  subroutine kill_net(this,&
       stderr)
    use Globals
    class(net), intent(inout) :: this
    integer,      intent(in   ) :: stderr
    !local
    logical :: rc
    integer :: res

    this%nnode = 0
    this%ncell = 0

    deallocate (&
         this%topol,&
         this%coord,&
         stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_dealloc, 'kill_net', &
         ' type net member topol, coord',res)

  end subroutine kill_net
  

  subroutine info_net(this,&
       lun)
    use Globals
    class(net), intent(in) :: this
    integer,      intent(in   ) :: lun
    !local
    integer :: j
    write(lun,*) 'Net Info'
    write(lun,*) 'Net Vertices = ', this%nnode
    write(lun,*) 'Net Edges    = ', this%ncell

  end subroutine info_net
  

  subroutine write2dat_net(this,&
       lun_err,&
       lun,&
       fname)
    use Globals
    class(net), intent(inout)       :: this
    integer,    intent(in   )       :: lun_err, lun
    character(len=*), intent(inout) :: fname
    !local
    logical :: rc
    integer :: res
    integer :: i
    
    write(lun,*)  this%nnode , '! Net Vertices' 
    write(lun,*)  this%ncell , '! Net Edges   '
    
    do i=1,this%nnode
       write(lun,*) this%coord(:,i)
    end do
    
    
    do i=1,this%ncell
       write(lun,*) this%topol(:,i)
    end do
    
  end subroutine write2dat_net


!!$  subroutine mesh2graph_tria(this, grid, stderr)
!!$    use GLobals
!!$    use Geometry
!!$    class(net),   intent(out) :: this
!!$    type(mesh),   intent(in ) :: grid
!!$    integer,      intent(in ) :: stderr
!!$    !local
!!$    logical :: rc
!!$    integer :: res
!!$    integer :: itria, itria_star,i,j,k, nstar
!!$    integer :: ncell, nnode
!!$    integer, allocatable :: net_topol(:,:), index_temp(:)
!!$
!!$    nnode = grid%ntria
!!$    ncell = sum(grid%tria_neigh(:)%nel)/2
!!$
!!$    allocate(net_topol(2,ncell),&
!!$         index_temp(nnode),&
!!$         stat=res)
!!$    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'mesh2net_tria', &
!!$         ' local array topol_net,index_temp',res)
!!$
!!$    i=0
!!$    do itria = 1,grid%ntria
!!$       nstar=grid%tria_neigh(itria)%nel
!!$       do j = 1,nstar
!!$          itria_star = grid%tria_neigh(itria)%el(j)
!!$          if (  itria_star > itria ) then
!!$             i=i+1
!!$             net_topol(1,i) = itria
!!$             net_topol(2,i) = itria_star
!!$          end if
!!$       end do
!!$    end do
!!$
!!$    do i=1,nnode
!!$       index_temp(i) = i
!!$    end do
!!$    call this%init(stderr, nnode, ncell,&
!!$         topol=net_topol, &
!!$         index=index_temp, &
!!$         coord=grid%bar_tria)
!!$    
!!$    deallocate(net_topol,index_temp, stat=res)
!!$    if(res .ne. 0) rc = IOerr(stderr, err_dealloc, 'mesh2net_tria', &
!!$         ' local array topol_net',res)
!!$  end subroutine mesh2graph_tria

  subroutine select(this, sub, stderr, toselect)
    use Globals
    class(net), intent(in   ) :: this
    class(net), intent(inout) :: sub
    integer,    intent(in   ) :: stderr
    logical,    intent(in   ) :: toselect(this%nnode)
    ! local
    logical :: rc
    integer :: res
    integer :: ivert, iedge,v1,v2
    integer :: nnode, ncell
    integer :: nnode_sub, ncell_sub
    integer, allocatable ::  topol_temp(:,:)
    integer, allocatable ::  index_temp(:)
    integer, allocatable ::  index_inv(:)
    real(kind=double), allocatable :: coord_temp(:,:)

    nnode = this%nnode
    ncell = this%ncell
 
    allocate(&
         topol_temp(2,ncell),&
         index_temp(nnode),&
         index_inv(nnode),&
         stat=res)

    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'select', &
         ' temp_array topol_temp, index_temp, index_inv, coord_temp',res)

    topol_temp = 0
    index_temp = 0
    index_inv  = 0
    coord_temp = 0.0d0
    
    nnode_sub = 0
    do ivert = 1, nnode
       if ( toselect(ivert) ) then
          nnode_sub               = nnode_sub + 1
          index_inv(ivert)        = nnode_sub
          index_temp(nnode_sub)   = ivert
       end if
    end do

    ncell_sub = 0
    do iedge = 1, ncell
       v1 = this%topol(1,iedge)
       v2 = this%topol(2,iedge)
       if ( toselect(v1) .and. toselect(v2) ) then
          ncell_sub = ncell_sub + 1
          topol_temp(1,ncell_sub) = index_inv(this%topol(1,iedge))
          topol_temp(2,ncell_sub) = index_inv(this%topol(2,iedge))
       end if
    end do
    

    call sub%init(stderr, nnode_sub ,ncell_sub,&
         topol_temp(:,1:ncell_sub))
    
    deallocate(topol_temp,&
         index_temp,&
         index_inv,&
         coord_temp,stat=res)

    if(res .ne. 0) rc = IOerr(stderr, err_dealloc, 'select', &
         ' temp_array topol_temp, index_temp, index_inv, coord_temp',res)

  end subroutine select


  subroutine init_adjmat(this,graph)
    use Globals
    class(adjmat),    intent(inout) :: this
    type(net), target, intent(in   ) :: graph
    
    this%graph => graph

    this%nrow=graph%ncell
    this%ncol=graph%nnode

    
  end subroutine init_adjmat

  subroutine kill_adjmat(this)
    use Globals
    class(adjmat),    intent(inout) :: this

    this%graph => null()

    this%nrow=0
    this%ncol=0

  end subroutine kill_adjmat

  subroutine info_adjmat(this,lun)
    use Globals
    class(adjmat), intent(in) :: this
    integer,        intent(in) :: lun

    call this%graph%info(lun)

  end subroutine info_adjmat
  
  
  subroutine MTxv(this,vec_in,vec_out,info,lun_err)
    use Globals
    implicit none
    class(adjmat),    intent(inout) :: this
    real(kind=double), intent(in   ) :: vec_in(this%nrow)
    real(kind=double), intent(inout) :: vec_out(this%ncol)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err

    ! local
    integer :: icell,i1,i2
    
    vec_out=zero
    do icell=1,this%graph%ncell
       i1=this%graph%topol(1,icell)
       i2=this%graph%topol(2,icell)
       vec_out(i1) = vec_out(i1) + vec_in(icell)
       vec_out(i2) = vec_out(i2) - vec_in(icell)
    end do
  end subroutine MTxv

  subroutine Mxv(this,vec_in,vec_out,info,lun_err)
    use Globals
    implicit none
    class(adjmat),     intent(inout) :: this
    real(kind=double), intent(in   ) :: vec_in(this%ncol)
    real(kind=double), intent(inout) :: vec_out(this%nrow)
    integer,           intent(inout) :: info
    integer,           intent(in   ) :: lun_err

    ! local
    integer :: icell,i1,i2

    info=0
    vec_out=zero
    do icell=1,this%graph%ncell
       i1=this%graph%topol(1,icell)
       i2=this%graph%topol(2,icell)
       vec_out(icell) = vec_in(i1) - vec_in(i2)
    end do
  end subroutine Mxv

  

  subroutine preassembly_stiff(this,stderr,stiff_base,assembler)
    use Globals
    use SparseMatrix
    class(adjmat),  intent(in   ) :: this
    integer, intent(in) :: stderr
    type(spmat), intent(inout) :: stiff_base
    integer, intent(inout) :: assembler(2,2,this%graph%ncell) 
    ! local
    logical :: rc
    integer :: res
    
    integer :: i,j,i1,i2,icell,nnode, ncell,nterm
    integer, allocatable :: connect(:),perm(:),iperm(:)
    

    ! local copies
    ncell = this%graph%ncell
    nnode = this%graph%nnode
    
    !
    ! count connection for node
    !
    allocate(connect(nnode),stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'select', &
         ' temp_array connect',res)
    connect=0
    do icell = 1, ncell
       i1=this%graph%topol(1,icell)
       i2=this%graph%topol(2,icell)
       connect(i1) =  connect(i1) + 1 
       connect(i2) =  connect(i2) + 1 
    end do
    
    ! 
    ! init matrix
    !
    nterm = 2 * ncell + nnode
    call stiff_base%init(stderr,nnode,nnode, nterm,&
         storage_system = 'csr',&
         is_symmetric=.true.)
    
    ! allcoate diaognal pointer
    !allocate (stiff_base%ind_diag(nnode),stat=res)
    !if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'select', &
    !     ' temp_array connect',res)
    !
    ! build ia
    !
    stiff_base%ia(1) = 1
    do i=1,nnode
       ! assembly ia
       stiff_base%ia(i+1)    = stiff_base%ia(i) + 1 + connect(i)
       ! add diagonal element 
       stiff_base%ja(stiff_base%ia(i)) = i
       ! mark diagonal pointer
       !stiff_base%ind_diag(i) = stiff_base%ia(i)
       !write(*,*) i, stiff_base%ia(i+1),connect(i)
    end do

    

    !
    ! build un sorted-ja 
    ! (use connect as scratch to count extra diagonal
    !  elements. It start from one to count the diagonal)
    !
    connect = 1
    do icell = 1, ncell       
       !
       i1 = this%graph%topol(1,icell)
       i2 = this%graph%topol(2,icell)
       !
       connect(i1) =  connect(i1) + 1 
       connect(i2) =  connect(i2) + 1 
       !
       stiff_base%ja( stiff_base%ia(i1)+connect(i1)-1 ) =  i2
       stiff_base%ja( stiff_base%ia(i2)+connect(i2)-1 ) =  i1

       !
       ! trija 
       !
       ! diagonal i1
       assembler(1,1,icell) = stiff_base%ia(i1)
       ! contribut to i1-i2
       assembler(2,1,icell) = stiff_base%ia(i2)+connect(i2)-1 
       ! diagonal i2
       assembler(2,2,icell) = stiff_base%ia(i2)
       ! contribut to i2-i1
       assembler(1,2,icell) = stiff_base%ia(i1)+connect(i1)-1 
    end do

 
    !
    ! sort ja and build permutation matrix
    !
    allocate(perm(nterm),iperm(nterm),stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'stiff_preassembly', &
         ' temp_array connect',res)    
    do j=1,nterm
       perm(j)=j
    end do

!!$    open (2222,file='sorting.dat')
    do i=1,nnode
       i1=stiff_base%ia(i)
       i2=stiff_base%ia(i+1)-1
!!$       write(2222,*) ' '
!!$       write(2222,*) i, connect(i)
!!$       write(2222,*) stiff_base%ja(i1:i2)

       call sort_matrix_line(connect(i), stiff_base%ja(i1:i2),perm(i1:i2))
!!$       write(2222,*) perm(i1:i2)
    end do
!!$    close(2222)

    do j=1,nterm
       iperm(perm(j))=j
    end do
    
    
    !
    ! re-assign trija
    !
    !open (1111,file='assembler.dat')
    do icell=1,ncell
       do j=1,2
          do i=1,2
             assembler(i,j,icell) = iperm(assembler(i,j,icell))
          end do
       end do
       !write(1111,*) icell,this%graph%topol(1:2,icell)
       do j=1,2
          do i=1,2
             !write(1111,*) assembler(i,j,icell), stiff_base%ja(assembler(i,j,icell))
          end do
       end do
       !write(1111,*)
    end do
    !close (1111)
    !
    ! re-assign ind_diag
    !
    !do i=1,nnode
    !   stiff_base%ind_diag(i)=iperm(stiff_base%ind_diag(i))
    !end do

    deallocate(connect,perm,iperm,stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_dealloc, 'select', &
         ' temp_array connect perm',res)


  contains
     subroutine sort_matrix_line(nnz,ja,perm)
      use Globals
      implicit none
      integer, intent(in   ) :: nnz
      integer, intent(inout) :: ja(nnz)
      integer, intent(inout) :: perm(nnz)
      !local 
      integer :: i,j, indx,isgn, itemp
      real(kind=double) :: rtemp

      !  Initialize.
      i    = 0
      indx = 0
      isgn = 0
      j    = 0
      do 
         call global_heapsort(nnz, indx, i,j,isgn)
         if (indx .gt. 0 ) then
            ! SWAP ELEMENT 

            ! swap column indeces
            itemp = ja(i)
            ja(i) = ja(j)
            ja(j) = itemp

            ! swap real nnzero coeff
            itemp   = perm(i)
            perm(i) = perm(j)
            perm(j) = itemp
         else if ( indx .lt. 0) then
            ! COMPARE
            isgn = 0
            if ( ja(i) .lt.  ja(j) ) isgn = -1
            if ( ja(i) .gt.  ja(j) ) isgn = 1
         else if ( indx .eq. 0 ) then
            exit
         end if
      end do
    end subroutine sort_matrix_line
  end subroutine preassembly_stiff

  subroutine assembly_stiff(this,weight,assembler,stiff)
    use Globals
    class(adjmat), intent(inout) :: this
    real(kind=double), intent(in) :: weight(this%graph%ncell)
    integer, intent(in) :: assembler(2,2,this%graph%ncell)
    type(spmat), intent(inout) :: stiff
    !local
    integer :: i,j,ind,icell
    real(kind=double) :: loc(2,2)
    
    loc(1,1) = one
    loc(2,2) = one
    loc(1,2) = -one
    loc(2,1) = -one
    
    
    stiff%coeff = zero
    do icell = 1,this%graph%ncell
       do j=1,2
          do i=1,2
             ind = assembler(i,j,icell)
             stiff%coeff(ind) = stiff%coeff(ind) + loc(i,j) * weight(icell)
          end do
       end do
    end do
  end subroutine assembly_stiff


!!$  subroutine build_edgegraph(this,edge_graph) 
!!$    implicit none
!!$    
!!$  
!!$    connect=0
!!$    do icell = 1, ncell
!!$       i1=this%graph%topol(1,icell)
!!$       i2=this%graph%topol(2,icell)
!!$       connect(i1) =  connect(i1) + 1 
!!$       connect(i2) =  connect(i2) + 1 
!!$    end do
!!$
!!$     
!!$
!!$    !
!!$    adj%ia(1) = 1
!!$    do i=1,nnode
!!$       ! assembly ia
!!$       adj%ia(i+1)    = adj%ia(i) + 1 + connect(i)
!!$    end do
!!$
!!$    
!!$
!!$    !
!!$    ! build un sorted-ja 
!!$    ! (use connect as scratch to count extra diagonal
!!$    !  elements. It start from one to count the diagonal)
!!$    !
!!$    connect = 1
!!$    do icell = 1, ncell       
!!$       !
!!$       i1 = this%graph%topol(1,icell)
!!$       i2 = this%graph%topol(2,icell)
!!$       !
!!$       connect(i1) =  connect(i1) + 1 
!!$       connect(i2) =  connect(i2) + 1 
!!$       !
!!$       stiff_base%ja( stiff_base%ia(i1)+connect(i1)-1 ) =  icell
!!$       stiff_base%ja( stiff_base%ia(i2)+connect(i2)-1 ) =  icell
!!$    end do
!!$
!!$    do inode = 1, nnode       
!!$       edge_in_star(:)=ja(ia(i), ia(i+1)+1)
!!$       m=0
!!$       do j=1,nel
!!$          do k=j, nel
!!$             m=m+1
!!$             topol(:,m) = edge_in_star(j,k)
!!$          end do
!!$       end do
!!$    end do
!!$
!!$    
!!$
!!$    
!!$
!!$
!!$
!!$       
!!$    ! 
!!$    ! init matrix
!!$    !
!!$    call edge_graph%init(stderr,nnode,ncell, 2*ncell,&
!!$         storage_system = 'csr',&
!!$         is_symmetric=.false.)

    


end module Graphs


