module Preconditioner
  use Globals
  use LinearOperator
  implicit none
  private
  public :: abs_prec,application,trans_prec,application_transpose
  type, abstract :: abs_prec
     !> Dimension of Domain and Codomain of Preconditioner
     !> They coincides since as preconditioner with consider 
     !> any linear linear operator "easy to be inverted"
     !> and the apply procedure defines the operation
     !>       vec_out = P^{-1} vec_in
     !> Default imposed to be negative to force redefinition
     integer :: nequ=-1
     !> Logical flag symmetric of preconditioner P
     !> Necessary to avoid use of non-symmetric preconditioner
     !> in PCG solver
     !> Defualti imposed to be
     logical :: is_symmetric=.false.
   contains
     procedure(application), deferred :: apply
     !procedure(assembly),    deferred :: build
  end type abs_prec
  abstract interface
     subroutine application(this,vec_in,vec_out,info,lun_err)
       use Globals
       import abs_prec
       class(abs_prec),   intent(inout) :: this
       real(kind=double), intent(in   ) :: vec_in(this%nequ)
       real(kind=double), intent(inout) :: vec_out(this%nequ)
       integer, optional, intent(inout) :: info
       integer, optional, intent(in   ) :: lun_err
     end subroutine application
          
  end interface

  !> Abstract declaration for preconditioner such that 
  !> we can compute 
  !>   x = prec^{-T} y 
  type, extends(abs_prec), abstract :: trans_prec
     ! No more info
   contains
     procedure(application_transpose), deferred :: apply_transpose
  end type trans_prec
  abstract interface
     subroutine application_transpose(this,vec_in,vec_out,info,lun_err)
       use Globals
       import trans_prec
       class(trans_prec), intent(inout) :: this
       real(kind=double), intent(in   ) :: vec_in(this%nequ)
       real(kind=double), intent(inout) :: vec_out(this%nequ)
       integer, optional, intent(inout) :: info
       integer, optional, intent(in   ) :: lun_err
     end subroutine application_transpose          
  end interface


  type, public :: array_prec
     !> Data type used to generate array of preconditioners
     class(abs_linop), pointer :: prec
  end type array_prec


  type, extends(abs_linop), public :: idprec
     !> Identity preconditioner
     !>  vec_out = Id vec_in
   contains
     !> Static constructor 
     !> (procedure public for type idprec)
     procedure, public, pass :: init => init_idprec
     !> static destructor
     !> (procedure public for type idprec)
     procedure, public, pass :: kill => kill_idprec
     !> Info procedure  sparse matrix
     !> (public procedure for type idprec)
     procedure, public, pass :: info => info_idprec
     !> Compute pvec = P^{-1} vec 
     !> (public procedure for type idprec)
     procedure, public,  pass :: Mxv => apply_idprec
  end type idprec
contains
  subroutine  init_idprec(this,nequ)
    implicit none
    class(idprec), intent(inout) :: this
    integer,        intent(in   ) :: nequ
    this%ncol = nequ
    this%nrow = nequ
    this%is_symmetric = .true.
  end subroutine init_idprec

  subroutine  kill_idprec(this)
    implicit none
    class(idprec), intent(inout) :: this    
   
    this%ncol = 0
    this%nrow = 0
    this%is_symmetric = .false.
  end subroutine kill_idprec

  subroutine  info_idprec(this,lun)
    implicit none
    class(idprec), intent(in) :: this 
    integer,        intent(in   ) :: lun

    write(lun,*) 'Nequ= ', this%nrow
  end subroutine info_idprec

  recursive subroutine apply_idprec(this,vec_in,vec_out,info,lun_err)
    use Globals
    class(idprec),   intent(inout) :: this
    real(kind=double), intent(in   ) :: vec_in(this%ncol)
    real(kind=double), intent(inout) :: vec_out(this%nrow)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err

    vec_out = vec_in
  end subroutine apply_idprec  
  
end module Preconditioner
