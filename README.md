### What is this repository for? ### 

This repository contains modules to handle linear algebra operations.
It contains the structures to handle sparse and dense matrices,
their sum, difference, products, block combinations.
It is designed to be self-contined and easy-to-use as much as possible. 
It contains:
   * Standard iterative solvers:
     * Krylov-based (PCG,BICGSTAB,MINRES,GMRES,FGMRES via hsl library. See external/hsl/ for details)
     * Stationary solver (GAUSS-SEIDEL, JACOBI)
   * Standard incomplete factorization (Incomplete Cholesky and LU factorization)
   * Wrap to algebraic multigrid solver AGMG (you need to [download](http://www.agmg.eu/)
     it yourself. See external/AGMG for details)
   * Wrap to BLAS and LAPACK subroutine for dense matrices (http://www.netlib.org/lapack/)
   * Wrap to ARPACK subroutines for eigenvalues computations (See external/arpack/ for details)

* Version
2.0

## How do I get set up?
### Dependencies
  * PROGRAMS (MANDATORY)
    * Fortran compiler compatible with FORTRAN 2003 ( tested with gfortran v>=4.8 )
    * Cmake ( version >= 2.8)  
  * EXTERNAL LIBRARIES (MANDATORY)
    * Blas/Lapack libraries
  * EXTERNAL REPOSITORIES 
    * Clone the following  MANDATORY repositories
      in the same directory      
```
git clone https://gitlab.com/enrico_facca/globals.git
```
    The results should be
    .
    ├── globals
    └── linear_algebra


    If you are interested in eigen-values/vectors
    computation clone via git-submodule the arpack repository
```
git submodule init
git submodule update
```
  
    
### Dependencies for fortran-python interface
  * f90wrap (https://github.com/jameskermode/f90wrap.git)


### Compile pure Fortran sources

In Unix-base dsystem use the classical Cmake commands to compile
libraries and programs:
```
  mkdir build/;
  cd build;
  cmake ../; 
  make
```

Compiling options are:
* RELEASE (default)
* DEBUG
* IEEE
* USER
that can be invoked adding the cmake flag

```
cmake -DBUILD_TYPE="compiling option" .. ;
```

### Troubleshooting

In case your gfortran compiler path is not "/usr/bin/gfortran" pass the absoulte path 

```
cmake -DMy_Fortran_Compiler="absolute path to gfortran" .. ;
```

In case of errors try to pass the path of the directories containg the
blas and lapack libraries using the cmake flag:

```
cmake -DBLAS_DIR="absolute path for blas library" -DLAPACK_DIR="absolute path for lapack library" ..; 
```

In order to diagnose the error sources use also 

```
cmake .. VERBOSE=1;
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact