program test_time
  use Globals
  use Timing

  real(kind=double), allocatable :: x(:)
  real(kind=double), allocatable :: y(:)
  real(kind=double), allocatable :: z(:)

  type(tim) :: cpu
  integer :: nargs,n,res
  real(kind=double) :: alpha
  character(len=256):: arg

  
  nargs = command_argument_count() 
  call  get_command_argument(1, arg,status=res)
  read(arg,*,iostat=res) n
  write(*,*) 'N=',n

  write(*,*) 'ALLOCATION'
  call cpu%init()
  call cpu%set('start')
  allocate(x(n),y(n),z(n))
  call cpu%set('stop')
  call cpu%info(6)

  call cpu%set('start')
  x=one
  y=one
  write(*,*) 'INITIALIZATION TO ONE'
  call cpu%set('stop')
  call cpu%info(6)

  write(*,*) 'INITIALIZATION TO RANGE'
  call cpu%set('start')
  do i=1,n
     x(i)=real(i)
  end do
  call cpu%set('stop')
  call cpu%info(6)


  
  write(*,*) 'FORTRAN ax+y'
  call cpu%set('start')
  y=alpha*x+y
  call cpu%set('stop')
  call cpu%info(6)

  write(*,*) 'BLAS ax+y'
  call cpu%set('start')
  call daxpy(n,alpha,x,1,y,1)
  call cpu%set('stop')
  call cpu%info(6)

  write(*,*) 'DEALLOCATION'
  call cpu%set('start')
  deallocate(x,y,z)
  call cpu%set('stop')
  call cpu%info(6)

  
  

end program test_time
  
  
