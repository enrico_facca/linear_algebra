program main
  use Globals
  use LinearOperator
  use Matrix
  use SimpleMatrix
  use SparseMatrix
  implicit none
  real(kind=double) :: x(2),y(2),z(2),t(2)
  type(scalmat),target :: id1,id2,id3,id4,eye_nx
  type(new_linop),target :: sum12,sum23,sum123_ok,sum123_ko

  type(array_linop) :: list(2)
  integer  :: j,k, n,nx,info
  
  integer :: i


  list(1)%linop=> id1
  list(2)%linop=> id2
  
  call id1%eye(2)
  id1%name='I1'
  call id2%eye(2)
  id2%name='I2'
  call id3%eye(2)
  id3%name='I3'
  call id4%eye(2)
  id4%name='I4'
  x=1.0d0
  y=1.0d0
  z=1.0d0

  write(*,*) ' Vector x =', x
  write(*,*) ' '

  write(*,*) ' M:= id1+2*id2+(3*id3+4*id4)'
  sum12= id1+2*id2+(3*id3+4*id4)
  call sum12%info(6)
  write(*,*) ' '
  
    
  call sum12%Mxv(x,t)
  write(*,*) ' M x =',t

end program main
