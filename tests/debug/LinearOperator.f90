module scrmod
  implicit none
  public :: setscr
  real(kind=8),allocatable :: scr(:)
  integer :: test
  
contains
  subroutine setscr(nscr)
    implicit none
    integer :: nscr

    if ( allocated(scr) ) then
       if ( size(scr)< nscr) then
          deallocate(scr)
          allocate(scr(nscr))
       end if
    else
       allocate(scr(nscr))
    end if

  end subroutine setscr
end module scrmod
  

module LinearOperator
  use scrmod
  implicit none
  
  private
  public :: abs_linop,multiplication
  type, abstract :: abs_linop   
     integer :: nrow=0
     integer :: ncol=0
     character(len=30) :: name='empty'
   contains
     !> Procedure for computation of (matrix) times (vector)
     procedure(multiplication), deferred :: Mxv
  end type abs_linop

  abstract interface
     !>-------------------------------------------------------------
     !> Abstract procedure defining the interface for a general
     !<-------------------------------------------------------------
     subroutine multiplication(this,vec_in,vec_out,info,lun_err)
       import abs_linop
       implicit none
       class(abs_linop), intent(inout) :: this
       real(kind=8), intent(in   ) :: vec_in(this%ncol)
       real(kind=8), intent(inout) :: vec_out(this%nrow)
       integer, optional, intent(inout) :: info
       integer, optional, intent(in   ) :: lun_err
     end subroutine multiplication

  end interface


  type, public :: array_linop
     !> Dimension (nmats)
     !> Array that contains the non-zero blocks
     class(abs_linop), pointer :: linop
  end type array_linop
  
  !>---------------------------------------------------------
  !> Structure variable for Identity matrix
  !> (rectangular case included)
  !>---------------------------------------------------------
  type, extends(abs_linop), public :: eye
   contains
     !> Static constructor 
     procedure, public, pass :: init => init_eye
     !> Compute matrix times vector operatoration
     procedure, public,  pass :: Mxv => apply_eye
  end type eye


  !>----------------------------------------------------------------
  !> Structure variable to build implicit matrix defined
  !> as composition and sum of linear operator
  !>----------------------------------------------------------------
  public :: add_linop,operator(*)
  type, extends(abs_linop) :: add_linop
     class(abs_linop) , pointer :: matrix_1
     class(abs_linop) , pointer :: matrix_2
     real(kind=8), allocatable  :: scr(:)
   contains
     procedure, public , pass:: Mxv => add_Mxv
  end type add_linop

 INTERFACE OPERATOR (+)
    module PROCEDURE mmsum
 END INTERFACE OPERATOR (+)
!!$  INTERFACE OPERATOR (*)
!!$     PROCEDURE rmprod
!!$  END INTERFACE OPERATOR (*)

  public :: linear_combination_linop, sumup,operator(+)
  type, extends(abs_linop) :: linear_combination_linop
     integer :: nlist
     type(array_linop), allocatable :: list (:)
     real(kind=8), allocatable  :: scr(:)
   contains
     procedure, public , pass:: Mxv => lin_comb_Mxv
     procedure, public , pass:: init =>init_lc_linop
  end type linear_combination_linop

  !INTERFACE OPERATOR (+)
  !   module PROCEDURE sumup
  !END INTERFACE OPERATOR (+)
  
  !>-------------------------------------------------
  !> Structure variable for Identity matrix
  !>------------------------------------------------
  type, extends(abs_linop), public :: zeromat
   contains
     !> Static constructor 
     !> (procedure public for type zeromat)
     procedure, public, pass :: init => init_zeromat
    
     !> Compute matlinopsrix times vector operatoration
     !> (public procedure for type zeromat)
     procedure, public,  pass :: Mxv => apply_zeromat
    
  end type zeromat

!!$  type, extends(abs_linop), public :: compose_linop
!!$     !> Flag if spmat has been initialized
!!$     logical :: is_initialized =.false.
!!$     !> Number of Linear Operator
!!$     integer :: nlinops     
!!$     !> Pointer to matrices
!!$     type(array_linop), allocatable :: linops(:)
!!$     !> Flag for transposition
!!$     character(len=1), allocatable :: direction(:)
!!$     !> Maximum of matrices dimensions
!!$     integer :: nscr
!!$     !> Scratch array in matrix vector multiplications
!!$     real(kind=8), allocatable :: scrin(:)
!!$     !> Scratch array in matrix vector multiplications
!!$     real(kind=8), allocatable :: scrout(:)
!!$   contains
!!$     !> static constructor
!!$     !> (procedure public for type compose_linop)
!!$     procedure, public, pass :: init => init_compose_linop
!!$     !> static destructor
!!$     !> (procedure public for type compose_linop)
!!$     procedure, public, pass :: kill => kill_compose_linop
!!$     !> Procedure to compute 
!!$     !>         y = M * x 
!!$     !> (public procedure for type blockmat)
!!$     procedure, public, pass :: Mxv => Mxv_compose_linop
!!$  end type compose_linop

 
contains
!!$subroutine init_compose_linop(this, lun_err, &
!!$       nlinops, linops,&
!!$       is_symmetric, triangular)
!!$    implicit none
!!$    !var
!!$    class(compose_linop), intent(inout) :: this
!!$    integer,            intent(in   ) :: lun_err
!!$    integer,            intent(in   ) :: nlinops
!!$    type(array_linop),  intent(in   ) :: linops(nlinops)
!!$    logical, optional,  intent(in   ) :: is_symmetric
!!$    character, optional,intent(in   ) :: triangular
!!$
!!$    ! local vars
!!$    logical :: rc
!!$    integer :: res
!!$    integer :: imat,nin,nout,nscr
!!$
!!$    this%nlinops      = nlinops
!!$   
!!$
!!$    !
!!$    ! check dimensions
!!$    !
!!$    nscr=0
!!$    do imat = nlinops,2,-1
!!$       nout = linops(imat)%linop%nrow 
!!$       nscr=max(nscr,nout)
!!$       nin = linops(imat-1)%linop%ncol 
!!$       nscr=max(nscr,nin)
!!$
!!$    end do
!!$    this%nscr=nscr
!!$    
!!$    
!!$    allocate(&
!!$         this%linops(nlinops),&
!!$         stat=res)
!!$     
!!$    this%linops      = linops
!!$
!!$    this%nrow = linops(1)%linop%nrow
!!$    this%ncol = linops(nlinops)%linop%ncol
!!$    
!!$    !
!!$    ! allocate scratch arrays for intermediate 
!!$    ! multiplication. We use the maximal dimensions
!!$    ! of the matrices
!!$    !
!!$    this%nscr = nscr
!!$    allocate(&
!!$         this%scrin(nscr),&
!!$         this%scrout(nscr),&
!!$         stat=res)
!!$    
!!$    
!!$
!!$  end subroutine init_compose_linop
!!$
!!$  !>-------------------------------------------------------------
!!$  !> Static destructor.
!!$  !> (procedure public for type compose_linop)
!!$  !> Free memory and diassociate pointers
!!$  !>
!!$  !> usage:
!!$  !>     call 'var'%init(lun_err)
!!$  !>
!!$  !> where:
!!$  !> \param[in] lun_err               -> integer. Error logical unit
!!$  !<-------------------------------------------------------------
!!$  subroutine kill_compose_linop(this, lun_err)
!!$
!!$    implicit none
!!$    !var
!!$    class(compose_linop),   intent(inout) :: this
!!$    integer,           intent(in   ) :: lun_err
!!$    ! local vars
!!$    logical :: rc
!!$    integer :: res
!!$
!!$    
!!$    this%nrow  = 0
!!$    this%ncol  = 0
!!$    
!!$    this%nlinops = 0
!!$    deallocate(&
!!$         this%linops,&
!!$         stat=res)
!!$   
!!$
!!$    this%nscr  = 0
!!$    deallocate(&
!!$         this%scrin,&
!!$         this%scrout,&
!!$         stat=res)
!!$    
!!$    
!!$  end subroutine kill_compose_linop
!!$  
!!$  !>-------------------------------------------------------------
!!$  !> Abstract procedure defining the interface for a general
!!$  !> matrix-vector multiplication
!!$  !>         vec_out = (M) times (vec_in)
!!$  !> (public procedure for class abs_matrix)
!!$  !> 
!!$  !> usage:
!!$  !>     call 'var'%Mxv(vec_in,vec_out,[info])
!!$  !>
!!$  !> where 
!!$  !> \param[in   ] vec_in          -> real, dimension('var'%ncol)
!!$  !>                                   vector to be multiplied
!!$  !> \param[inout] vec_out         -> real, dimension('var'%nrow)
!!$  !>                                    vector (M) times (vec_in) 
!!$  !> \param[in   ] (optional) info -> integer. Info number
!!$  !>                                  in case of error   
!!$  !<-------------------------------------------------------------
!!$  recursive subroutine Mxv_compose_linop(this,vec_in,vec_out,info,lun_err)
!!$    implicit none
!!$    class(compose_linop), intent(inout) :: this
!!$    real(kind=8),  intent(in   ) :: vec_in(this%ncol)
!!$    real(kind=8),  intent(inout) :: vec_out(this%nrow)
!!$    integer, optional,  intent(inout) :: info
!!$    integer, optional,  intent(in   ) :: lun_err
!!$           
!!$    ! local
!!$    integer :: imat,nin,nout,info_loc
!!$
!!$
!!$    this%scrin(1:this%ncol)  = vec_in(1:this%ncol)
!!$    this%scrout(1:this%ncol) = zero
!!$    do imat  = this%nlinops,1,-1
!!$       nin  = this%linops(imat)%linop%ncol
!!$       nout = this%linops(imat)%linop%nrow
!!$       info_loc=0
!!$       call this%linops(imat)%linop%Mxv( &
!!$            this%scrin(1:nin),&
!!$            this%scrout(1:nout),&
!!$            info=info_loc,&
!!$            lun_err=lun_err)
!!$       if ( ( info_loc .ne. 0) .and. present(info) ) then
!!$          info=info_loc
!!$          return
!!$       end if
!!$       this%scrin(1:nout) = this%scrout(1:nout)
!!$    end do
!!$
!!$    vec_out = this%scrout(1:this%nrow) 
!!$
!!$  end subroutine Mxv_compose_linop
!!$
!!$   function rmprod(alpha,matrix) result(this)
!!$    implicit none
!!$    real(kind=8),        intent(in) :: alpha
!!$    class(abs_linop), target, intent(in) :: matrix
!!$    type(compose_linop)           :: this
!!$
!!$    this%matrix_1  => matrix
!!$    this%alpha = alpha
!!$    this%type = '*r'
!!$    
!!$    this%nrow = matrix%nrow
!!$    this%ncol = matrix%ncol
!!$
!!$    this%is_symmetric = matrix%is_symmetric
!!$  end function rmprod

  
   !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type zeromat)
  !>
  !> usage:
  !>     call 'var'%init( nrow, [ncol] )
  !>
  !> where:
  !> \param[in] nrow          -> integer. Number of rows 
  !> \param[in,optional] ncol -> integer. Number of columns.
  !>                             If not passed ncol=nrow
  !<-------------------------------------------------------------
  subroutine  init_zeromat(this,nrow,ncol)
    implicit none
    class(zeromat),  intent(inout) :: this
    integer,         intent(in   ) :: nrow
    integer,optional,intent(in   ) :: ncol
    
    this%nrow = nrow
    if ( present(ncol) )  then
       this%ncol = ncol
    else
       this%ncol = nrow
       
    end if
  end subroutine init_zeromat

  

 
  !>-------------------------------------------------------------
  !> Matrix-vector multiplication procedure
  !>         vec_out = (M) times (vec_in)
  !> (public procedure for class abs_matrix)
  !> 
  !> usage:
  !>     call 'var'%Mxv(vec_in,vec_out,[info])
  !>
  !> where 
  !> \param[in   ] vec_in            -> real, dimension('var'%ncol)
  !>                                   vector to be multiplied
  !> \param[inout] vec_out           -> real, dimension('var'%nrow)
  !>                                    vector (M) times (vec_in) 
  !> \param[in   ] (optional) info    -> integer. Info number
  !>                                    in case of error 
  !> \param[in   ] (optional) lun_err -> integer. Info number
  !>                                    in case of error 
  !<-------------------------------------------------------------
  subroutine apply_zeromat(this,vec_in,vec_out,info,lun_err)
    class(zeromat),   intent(inout) :: this
    real(kind=8), intent(in   ) :: vec_in(this%ncol)
    real(kind=8), intent(inout) :: vec_out(this%nrow)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err

    vec_out = 0.0d0

    if (present(info)) info=0
    
  end subroutine apply_zeromat

  
  
    
  !>------------------------------------------------------
  !> Function that give two linear operator A1 and A2
  !> defines, implicitely, the linear operator
  !> A=A1+A2
  !> (public procedure for class add_linop)
  !> 
  !> usage:
  !>     'var' = A1 + A2
  !<-------------------------------------------------------------
  function mmsum(matrix_1,matrix_2) result(this)
    implicit none
    class(abs_linop), target, intent(in) :: matrix_1
    class(abs_linop), target, intent(in) :: matrix_2
    class(add_linop), save  :: this
    ! local
    integer :: res
    character(len=20) :: n1,n2

    if (matrix_1%nrow .ne. matrix_2%nrow)  &
         write(*,*) 'Error mmproc dimension must agree '
    if (matrix_1%ncol .ne. matrix_2%ncol)  &
         write(*,*) 'Error mmproc dimension must agree '


    this%matrix_1 => matrix_1
    this%matrix_2 => matrix_2
    
    this%nrow = matrix_1%nrow
    this%ncol = matrix_2%ncol

    this%name=etb(matrix_1%name)//'+'//etb(matrix_2%name)
    
    write(*,*) 'Sum Matrix initialization '    
    write(*,*) 'M1  : ',this%matrix_1%name
    write(*,*) 'M2  : ',this%matrix_2%name
    write(*,*) 'sum : ',this%name,this%nrow

    call setscr(this%nrow)
    allocate(this%scr(this%nrow),stat=res)
  contains
    function etb(strIn) result(strOut)
      implicit none
      ! vars
      character(len=*), intent(in) :: strIn
      character(len=len_trim(adjustl(strIn))) :: strOut

      strOut=trim(adjustl(strIn))
    end function etb
  end function mmsum

  recursive subroutine add_Mxv(this,vec_in,vec_out,info,lun_err)
    use scrmod
    implicit none
    class(add_linop),  intent(inout) :: this
    real(kind=8), intent(in   ) :: vec_in(this%ncol)
    real(kind=8), intent(inout) :: vec_out(this%nrow)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err

    write(*,*) 'Matrix vector multipliction',&
         'matrix:',this%name,&
         'M1: ',this%matrix_1%name,&
         'M2: ',this%matrix_2%name,this%nrow
    select type (mat=>this%matrix_1)
    type is (add_linop)
       write(*,*) 'is allocated(mat%scr) ?', allocated(mat%scr)
    end select
    
    call this%matrix_1%Mxv(vec_in,vec_out,info=info,lun_err=lun_err)
    call this%matrix_2%Mxv(vec_in,scr,info=info,lun_err=lun_err)
    vec_out=vec_out+scr
  end subroutine add_Mxv

  subroutine init_lc_linop(this,nlist,list)
    use scrmod
    implicit none
    class(linear_combination_linop),intent(inout) :: this
    integer,           intent(in) :: nlist
    type(array_linop) , intent(in) :: list(nlist)

    this%nrow = list(1)%linop%nrow
    this%ncol = list(1)%linop%ncol

    call setscr(this%nrow)

    if (.not. allocated(this%scr)) allocate(this%scr(this%nrow))

    if (.not. allocated(this%scr)) allocate(this%list(nlist))

    this%nlist = nlist
    this%list = list 
  end subroutine init_lc_linop
    
  function sumup(matrix_1,matrix_2) result(this)
    implicit none
    class(abs_linop), target, intent(in) :: matrix_1
    class(abs_linop), target, intent(in) :: matrix_2
    type(linear_combination_linop) :: this
    ! local
    integer :: i
    type(array_linop), allocatable :: temp(:)
    
    select type (matrix_1)
    class is (linear_combination_linop)
       this%nlist = matrix_1%nlist+1
       allocate(temp(matrix_1%nlist+1))
       do i=1,matrix_1%nlist
          temp(i)%linop => matrix_1%list(i)%linop
       end do
       temp(matrix_1%nlist+1)%linop=> matrix_2
       call this%init(matrix_1%nlist+1,temp)
       deallocate(temp)
    class DEFAULT
       this%nlist = 2
       allocate(temp(2))
       temp(1)%linop=>matrix_2
       temp(2)%linop=>matrix_2
       call this%init(2,temp)
       deallocate(temp)
    end select

!!$    select type (matrix_2)
!!$    class is (linear_combination_linop)
!!$       this%nlist = matrix_2%nlist+1
!!$       allocate(temp(matrix_2%nlist+1))
!!$       do i=1,matrix_2%nlist
!!$          temp(i)%linop => matrix_2%list(i)%linop
!!$       end do
!!$       temp(matrix_2%nlist+1)%linop=> matrix_1
!!$       call this%init(matrix_2%nlist+1,temp)
!!$       deallocate(temp)
!!$    class DEFAULT
!!$       this%nlist = 2
!!$       allocate(temp(2))
!!$       temp(1)%linop=>matrix_2
!!$       temp(2)%linop=>matrix_2
!!$       call this%init(2,temp)
!!$       deallocate(temp)
!!$    end select
    


  end function sumup

  recursive subroutine lin_comb_Mxv(this,vec_in,vec_out,info,lun_err)
    use scrmod
    implicit none
    class(linear_combination_linop),  intent(inout) :: this
    real(kind=8), intent(in   ) :: vec_in(this%ncol)
    real(kind=8), intent(inout) :: vec_out(this%nrow)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err
    !local
    integer :: i
    vec_out=0.0d0
    do i=1, this%nlist
       call this%list(i)%linop%Mxv(vec_in,this%scr)
       vec_out=vec_out+this%scr
    end do
    
  end subroutine lin_comb_Mxv

  
  subroutine  init_eye(this,nrow)
    implicit none
    class(eye),      intent(inout) :: this
    integer,         intent(in   ) :: nrow
     this%nrow = nrow
    this%ncol = nrow
  end subroutine init_eye
  
  subroutine apply_eye(this,vec_in,vec_out,info,lun_err)
    class(eye),   intent(inout) :: this
    real(kind=8), intent(in   ) :: vec_in(this%ncol)
    real(kind=8), intent(inout) :: vec_out(this%nrow)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err
    ! local
    integer :: mindim

    vec_out = vec_in
    if (present(info)) info=0

  end subroutine apply_eye


  



end module LinearOperator




program main
  use LinearOperator
  implicit none
  real(kind=8) :: x(2),y(2),z(2),t(2)
  type(eye),target:: id1,id2,id3,id4,id5,id6,id7,id8
  type(eye),allocatable :: multi_id(:)
  type(add_linop), target:: sum12,sum23,sum123_ok
  type(add_linop), target  :: sum123_ko ,sum4,sum5
  !type(linear_combination_linop),target :: newsum,newsum1,newsum2,newsum3

  integer :: i,n
  call id1%init(2)
  id1%name='I1'
  call id2%init(2)
  id2%name='I2'
  call id3%init(2)
  id3%name='I3'
  call id4%init(2)
  id4%name='I4'
  call id5%init(2)
  id5%name='I5'

  n=10
  allocate(multi_id(n))
  do i=1,n
     call multi_id(i)%init(2)
  end do
  
  x=1.0d0
  y=1.0d0
  z=1.0d0

!!$  write(*,*) ' Vector x =', x
!!$  call id1%Mxv(x,t)
!!$  write(*,*) ' Vector t = I1 *x', t
!!$
!!$  write(*,*) ' '
!!$
  sum12 = id1 + id2
  call sum12%Mxv(x,t)
  write(*,*) ' Vector t = (I1 +I2) *x', t
  !write(*,*) ' ',associated(sum12%matrix_1,id1)
!!$
!!$  sum23 = id2 + id3
!!$  sum123_ok = id1 + sum23
!!$  call sum123_ok%Mxv(x,t)
!!$  write(*,*) ' Vector t = ( I1 + (I2 + I3) )*x', t

!  newsum = sumup( id2,id3)
!!$  newsum = id1+id2+id3+id4
!!$  call newsum%Mxv(x,t)
!!$  write(*,*) ' Vector t = ( I1 +I2 + I3+I4) )*x', t
!!$
!!$  newsum1 = id1+id2
!!$  call newsum1%Mxv(x,t)
!!$  write(*,*) ' Vector t = ( I1 + I3) )*x', t
!!$
!!$  newsum2 = id3+id4
!!$  call newsum2%Mxv(x,t)
!!$  write(*,*) ' Vector t = ( I3 + I4) )*x', t
!!$
!!$  newsum3 = newsum1+newsum2
!!$  call newsum3%Mxv(x,t)
!!$  write(*,*) ' Vector t = ( ( I1 + I2) + (I3+I4) )*x', t

!!$  write(*,*) ' '
!!$  sum123_ko = id1 + id2 + id3
!!$  write(*,*) ' ',associated(sum123_ko%matrix_1),sum123_ko%matrix_1%name,sum123_ko%matrix_2%name
!!$  call sum123_ko%Mxv(x,t)
!!$  write(*,*) ' Vector t = ( I1 +I2 + I3) *x', t

  
  
  
end program main
