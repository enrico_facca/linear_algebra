program main
  use LinearOperator
  use Matrix
  use SparseMatrix
  implicit none
  real(kind=8) :: x(2),y(2),z(2),t(2)
  type(eye) :: id1,id2,id3,id4
  type(linear_combination_linop) :: sum12,sum23,sum123_ok,sum123_ko 
  integer :: i
  call id1%init(2)
  id1%name='I1'
  call id2%init(2)
  id2%name='I2'
  call id3%init(2)
  id3%name='I3'
  call id4%init(2)
  id3%name='I4'
  x=1.0d0
  y=1.0d0
  z=1.0d0

  write(*,*) ' Vector x =', x
  call id1%Mxv(x,t)
  write(*,*) ' Vector t = I1 *x', t

  write(*,*) ' '

  sum12 = id1 + id2
  call sum12%Mxv(x,t)
  write(*,*) ' Vector t = (I1 +I2) *x', t

  write(*,*) ' '

  sum23 = id2 + id3
  sum123_ok = id1 + sum23
  call sum123_ok%Mxv(x,t)
  write(*,*) ' Vector t = ( (I1 +I2) + I3) *x', t


  write(*,*) ' '
  sum123_ko = id1 + id2 + id3
  call sum123_ko%Mxv(x,t)
  write(*,*) ' Vector t = ( I1 +I2 + I3) *x', t

  write(*,*) ' '
  sum123_ko = id1 + id2 + id3 + id4
  call sum123_ko%Mxv(x,t)
  write(*,*) ' Vector t = ( I1 +I2 + I3 + I4) *x', t



  
  
  
end program main
