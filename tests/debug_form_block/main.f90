program main
  use Globals
  use LinearOperator
  use Matrix
  use SimpleMatrix
  use SparseMatrix
  implicit none
  real(kind=double) :: x(2),y(2),z(2),t(2)
  type(scalmat),target :: id1,id2,id3,id4,eye_nx
  type(new_linop),target :: sum12,sum23,sum123_ok,sum123_ko
  type(spmat), target :: matrixT,matrixTbis,matrixTns,formed,combine
  type(block_linop) :: matrixA,matrixAns
  type(scalmat),target :: extra_diagonal_nx
  integer, allocatable :: block_structure(:,:)
  real(kind=double) ,allocatable :: alphas(:)

  type(array_linop) :: list(2)
  integer  :: j,k, n,nx,info
  real(kind=double) :: tol, sigma,h,h2,dl,dd,du,rho

  
  integer :: i


  list(1)%linop=> id1
  list(2)%linop=> id2
  
  call id1%eye(2)
  id1%name='I1'
  call id2%eye(2)
  id2%name='I2'
  call id3%eye(2)
  id3%name='I3'
  call id4%eye(2)
  id4%name='I4'
  x=1.0d0
  y=1.0d0
  z=1.0d0

  write(*,*) ' Vector x =', x
  write(*,*) ' '

  write(*,*) ' M=I1*I2*I3'
  sum12= id1+2*id2+(3*id3+4*id4)
  call sum12%info(6)
  write(*,*) ' '
  
    
  call sum12%Mxv(x,t)
  write(*,*) ' M x =',t

  stop
!!$
!!$  write(*,*) ' '
!!$  
!!$  write(*,*) ' M=I1-I2'
!!$  sum23 = id1 - id2
!!$  call sum23%Mxv(x,t)
!!$  write(*,*) ' M x =',t
!!$
!!$  write(*,*) ' '
!!$  
!!$  write(*,*) ' M=I1+I2*I3'
!!$  write(*,*) loc(id1),loc(id2),loc(id3)
!!$  sum23 = id1+id2 * id3
!!$  write(*,*) ' outmain'
!!$  do i=1,sum23%npairs
!!$     write(*,*) sum23%list_of_pairs(i)%name,&
!!$          loc(sum23%list_of_pairs(i)%lhs),&
!!$          loc(sum23%list_of_pairs(i)%rhs)
!!$  end do
!!$  write(*,*) ' ',sum23%npairs
!!$  call sum23%info(6)
!!$  do i=1,sum23%npairs
!!$     write(*,*)  sum23%list_of_pairs(i)%name,&
!!$          loc(sum23%list_of_pairs(i)%lhs),&
!!$          loc(sum23%list_of_pairs(i)%rhs)
!!$  end do
!!$  write(*,*) ' M x =',t

  
  
  
  call sum123_ko%Mxv(x,t)
  write(*,*) ' M x =',t

  stop
  
  write(*,*) ' '
  write(*,*) ' M=I1-I2+I3'
  sum123_ko = id1 - id2 + id3 
  call sum123_ko%Mxv(x,t)
  write(*,*) ' M x =',t
  
  write(*,*) ' '
  write(*,*) ' M=I1-I2-I3-I4'
  sum123_ko = id1 - id2 - id3 + id4
  call sum123_ko%Mxv(x,t)
  write(*,*) ' M x =',t

  stop 
  ! not working
  !write(*,*) ' '
  !sum123_ko = id1 - (id2 + id3)
  !call sum123_ko%Mxv(x,t)
  !write(*,*) ' Vector t = ( I1 -I2 + I3) *x', t

  nx = 10
  n = nx*nx
  !     Mesh parameter
  h   = one / dble(nx+1)
  h2  = h*h

  !     allcoate work space for assembly block linear operator 
  allocate(block_structure(3,nx+2*(nx-1)),alphas(nx+2*(nx-1)))

  !     set constant for simmetric case      
  dd  = 4.0D+0 / h2 
  dl  = -one/h2
  du  = -one/h2

  call matrixT%init(6,nx,nx,nx+2*(nx-1)-1,&
       storage_system='csr',&
       is_symmetric=.True.)
  matrixT%ia(1)=1
  matrixT%ja(1)=1
  matrixT%coeff(1)=dd
!!$  matrixT%ja(2)=2
!!$  matrixT%coeff(2)=dl
  matrixT%ia(2)=2
  do i=2,nx-1
     matrixT%ia(i+1)=matrixT%ia(i)+3

     matrixT%ja(matrixT%ia(i):matrixT%ia(i+1)-1)=(/i-1,i,i+1/)
     matrixT%coeff(matrixT%ia(i):matrixT%ia(i+1)-1)=&
          (/dl,dd,du/)

  end do
  k=matrixT%ia(nx)
  matrixT%ja   (k)=nx-1
  matrixT%coeff(k)=dl
  matrixT%ja   (k+1)=nx
  matrixT%coeff(k+1)=dd
  matrixT%ia(nx+1)=k+1

  call matrixT%write(777)

  !
  ! remove last diagonal entries
  !
  call matrixTbis%init(6,nx,nx,nx+2*(nx-1)-1,&
       storage_system='csr',&
       is_symmetric=.True.)
  matrixTbis%ia(1)=1
  matrixTbis%ja(1)=1
  matrixTbis%coeff(1)=dd
  matrixTbis%ja(2)=2
  matrixTbis%coeff(2)=dl
  matrixTbis%ia(2)=3
  do i=2,nx-1
     matrixTbis%ia(i+1)=matrixTbis%ia(i)+3

     matrixTbis%ja(matrixTbis%ia(i):matrixTbis%ia(i+1)-1)=(/i-1,i,i+1/)
     matrixTbis%coeff(matrixTbis%ia(i):matrixTbis%ia(i+1)-1)=&
          (/dl,dd,du/)

  end do
  matrixTbis%ja   (nx+2*(nx-1)-1)=nx-1
  matrixTbis%coeff(nx+2*(nx-1)-1)=dl
  matrixTbis%ia(nx+1)=nx+2*(nx-1)

  call matrixTbis%write(888)
  
  sum12 = matrixT + matrixTbis
  !call combine%form_linear_combination(info,6,sum12)
  !call combine%write(999)
  
  
   call eye_nx%eye(nx)
   call extra_diagonal_nx%init(nx,-one/h2)
   
  list(1)%linop=>  matrixT
  list(2)%linop=>  extra_diagonal_nx

  block_structure(:,1)=(/1,1,1/)
  alphas(1)=1.0d0
  block_structure(:,2)=(/2,1,2/)
  alphas(2)=-1.0d0
  i=2
  do k= 2, nx-1
     ! lower diagonal -I
     i=i+1
     block_structure(:,i)=(/2,k,k-1/)
     alphas(i)=-1.0d0
     ! diagonal = T
     i=i+1
     block_structure(:,i)=(/1,k,k/)
     alphas(i)=1.0d0
     ! upper diagonal= -I 
     i=i+1
     block_structure(:,i)=(/2,k,k+1/)
     alphas(i)=-1.0d0
  end do
  i=i+1
  block_structure(:,i)=(/2,nx,nx-1/)
  alphas(i)=-1.0d0
  i=i+1
  block_structure(:,i)=(/1,nx,nx/)
  alphas(i)=1.0d0

  alphas=alphas*(nx+1)*(nx+1)


  call matrixA%init(6,2, list(1:2),&
       nx, nx, &
       nx+2*(nx-1), block_structure, &
       is_symmetric=.True.)


  call matrixAns%init(6,2, list(1:2),&
       nx, nx, &
       nx+2*(nx-1), block_structure, &
       is_symmetric=.False.)





end program
