add_executable(test_arpack test_arpack/main.f)
target_link_libraries(test_arpack linalg arpack lapack blas)

add_executable(debug_sum_difference debug_sum_difference/main.f90)
target_link_libraries(debug_sum_difference linalg lapack blas )

add_executable(test_time test_time/main.f90)
target_link_libraries(test_time globals blas)

